﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidTest : MonoBehaviour
{
    public Transform rootCam, objCam;
    Transform cam;
    Rigidbody rb;
    Vector3 accel;
    Vector3 fromCam = new Vector3(0f, 1f, -3f);
    Vector3 toCam = new Vector3(0f, 3f, -8f);
    float t = 0.5f, dis1, dis2, newproc;

    void Start()
    {
        cam = Camera.main.transform;
        rb = GetComponent<Rigidbody>();
        cam.localPosition = Vector3.Lerp(fromCam, toCam, t);

    }

    void Update()
    {
        //print(Input.acceleration);   
        //print(Input.deviceOrientation.ToString());    
        //print(Input.touchCount);
        /*if(Input.touchCount>0)
        {
            print(Input.GetTouch(0).position);
            print(Input.GetTouch(0).deltaPosition);
            print(Input.GetTouch(0).deltaTime);
            print(Input.GetTouch(0).phase);
        }*/

        rootCam.position = transform.position;
        accel = Input.acceleration;
        rb.velocity = rootCam.transform.TransformDirection(accel.x * 6, rb.velocity.y, (accel.y + 0.6f) * 9);

        if(Input.touchCount==1)
        {
            if(Input.GetTouch(0).phase==TouchPhase.Moved)
            {
                Vector2 move = Input.GetTouch(0).deltaPosition;

                if(Mathf.Abs(move.x)>0.4f)
                {
                    rootCam.Rotate(0, move.x, 0, Space.World);
                }

                if(Mathf.Abs(move.y)>0.4f)
                {
                    Vector3 rot = objCam.localEulerAngles;
                    objCam.Rotate(move.y, 0, 0, Space.Self);
                    if(objCam.localEulerAngles.x<-10 || objCam.localEulerAngles.x>40)
                    {
                        objCam.localEulerAngles = rot;
                    }
                }
            }
        }
        else if(Input.touchCount==2)
        {
            if(Input.GetTouch(0).phase==TouchPhase.Began || Input.GetTouch(1).phase == TouchPhase.Began)
            {
                dis1 = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                dis2 = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);

                if(dis1>dis2)
                {
                    // dis1 = 10    dis2 = 5
                    //                           0.1 
                    //                      50
                    //                                   0.5
                    //              0.5
                    float proc = 1f - dis2 / (dis1 / 100f) * 0.01f;
                    //          1
                    newproc = t + proc;
                }
                else if(dis1 < dis2)
                {
                    float proc = 1f - dis1 / (dis2 / 100f) * 0.01f;
                    newproc = t - proc;
                }

                newproc = Mathf.Clamp01(newproc);
                // 0.3 -> 0.3
                // -0.4 -> 0
                // 5 -> 1

                if(Mathf.Abs(t-newproc)>0.02f)
                {
                    if(t<newproc)
                    {
                        t += 0.02f;
                    }
                    else if(t>newproc)
                    {
                        t -= 0.02f;
                    }
                    cam.localPosition = Vector3.Lerp(fromCam, toCam, t);
                }
            }
        }

        if(transform.position.y<-10)
        {
            transform.position = Vector3.one;
        }
    }

    private void OnMouseDown()
    {
        rb.AddForce(0, 300, 0);
    }

}
