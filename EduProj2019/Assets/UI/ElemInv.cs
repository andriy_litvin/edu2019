﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// скрипт на кожен елемент інвентаря 
public class ElemInv : MonoBehaviour
{
    public Image img;   // посилання на катринку предмета в інвентарі 
    public int count;   // кількість предметів даного виду
    public string nameElem;   // назва предмету
    Text countText;     // посилання та текст для відображення кількості предметів 

    void Start()
    {
        countText = GetComponentInChildren<Text>();
        countText.text = count.ToString();
        if(!img.sprite)
        {
            img.color = new Color(1, 1, 1, 0);
            countText.text = "";
            count = 0;
        }
    }

    // початок перетягування
    public void BeginDrag()
    {
        // якщо ми натиснули на об'єкт інвентаря з картинкою, то можна починати перетягування 
        if (img.sprite)
        {
            MouseInv.mouse.imgMouse.sprite = img.sprite;
            MouseInv.mouse.imgMouse.color = new Color(1, 1, 1, 1);
            MouseInv.mouse.SetCount(count);
            img.color = new Color(1, 1, 1, 0);
            img.sprite = null;
            SetCount(0);
            MouseInv.mouse.isMove = true;
            MouseInv.mouse.nameElem = nameElem;
            MouseInv.mouse.elem = this;
        }
    }

    // завершення перетягування
    public void EndDrag()
    {
        if(MouseInv.mouse.isMove && MouseInv.mouse.imgMouse.sprite)
        {
            img.sprite = MouseInv.mouse.imgMouse.sprite;
            MouseInv.mouse.imgMouse.color = new Color(1, 1, 1, 0);
            img.color = new Color(1, 1, 1, 1);
            MouseInv.mouse.imgMouse.sprite = null;
            SetCount(MouseInv.mouse.count);
            MouseInv.mouse.SetCount(0);
            nameElem = MouseInv.mouse.nameElem;
        }
    }

    // метод кидання об'єкту після перетягування
    public void Drop()
    {
        if(MouseInv.mouse.isMove)
        {
            if(img.sprite)
            {
                MouseInv.mouse.elem = this;
                Sprite temp = img.sprite;
                int tempCount = count;
                SetCount(MouseInv.mouse.count);
                MouseInv.mouse.SetCount(tempCount);
                img.sprite = MouseInv.mouse.imgMouse.sprite;
                MouseInv.mouse.imgMouse.sprite = temp;
                MouseInv.mouse.isMove = false;
                string tempName = MouseInv.mouse.nameElem;
                MouseInv.mouse.nameElem = nameElem;
                nameElem = tempName;
            }
            else if(MouseInv.mouse.imgMouse.sprite)
            {
                img.sprite = MouseInv.mouse.imgMouse.sprite;
                MouseInv.mouse.imgMouse.color = new Color(1, 1, 1, 0);
                img.color = new Color(1, 1, 1, 1);
                MouseInv.mouse.imgMouse.sprite = null;
                SetCount(MouseInv.mouse.count);
                MouseInv.mouse.SetCount(0);
                nameElem = MouseInv.mouse.nameElem;
                MouseInv.mouse.isMove = false;
            }
        }
    }

    // натиснення на елемент інвентаря
    public void Click()
    {
        if(MouseInv.mouse.imgMouse.sprite)
        {
            if(img.sprite)
            {
                MouseInv.mouse.elem = this;
                Sprite temp = img.sprite;
                int tempCount = count;
                SetCount(MouseInv.mouse.count);
                MouseInv.mouse.SetCount(tempCount);
                img.sprite = MouseInv.mouse.imgMouse.sprite;
                MouseInv.mouse.imgMouse.sprite = temp;
                string tempName = MouseInv.mouse.nameElem;
                MouseInv.mouse.nameElem = nameElem;
                nameElem = tempName;
            }
            else 
            {
                img.sprite = MouseInv.mouse.imgMouse.sprite;
                MouseInv.mouse.imgMouse.color = new Color(1, 1, 1, 0);
                img.color = new Color(1, 1, 1, 1);
                MouseInv.mouse.imgMouse.sprite = null;
                SetCount(MouseInv.mouse.count);
                MouseInv.mouse.SetCount(0);
                nameElem = MouseInv.mouse.nameElem;
            }
        }
    }

    // корекція текстового поля, не відображаємо вміст якщо кількість предметів = 0
    public void SetCount(int count)
    {
        this.count = count;
        countText.text = count == 0 ? "" : count.ToString();
    }
}
