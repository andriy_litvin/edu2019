﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvSys : MonoBehaviour
{
    public static InvSys sys;
    public ElemInv[] elems;

    void Awake()
    {
        elems = GetComponentsInChildren<ElemInv>();
        sys = this;
    }

    public void AddToInventar(Sprite sprite, int count, string nameObj)
    {
        int countElemsInv = elems.Length;
        for(int i=0; i<countElemsInv; i++)
        {
            if(!elems[i].img.sprite)
            {
                elems[i].img.sprite = sprite;
                elems[i].SetCount(count);
                elems[i].nameElem = nameObj;
                elems[i].img.color = new Color(1, 1, 1, 1);
                return;
            }
        }
    }

}
