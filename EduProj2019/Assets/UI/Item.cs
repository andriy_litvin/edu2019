﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// скрипт для додавання елементів до інвентаря через інтерфейс
public class Item : MonoBehaviour
{
    public GameObject winMess;
    public int count;
    Text countText;
    Image img;

    // Start is called before the first frame update
    void Start()
    {
        img = GetComponent<Image>();
        countText = GetComponentInChildren<Text>();
        countText.text = count.ToString();
    }

    public void Click()
    {
        int countElemInv = InvSys.sys.elems.Length;
        for(int i =0; i<countElemInv; i++)
        {
            if(InvSys.sys.elems[i].count>0 && InvSys.sys.elems[i].nameElem == name)
            {
                InvSys.sys.elems[i].SetCount(InvSys.sys.elems[i].count+count);
                gameObject.SetActive(false);
                return;
            }
        }

        for (int i = 0; i < countElemInv; i++)
        {
            if(InvSys.sys.elems[i].count == 0)
            {
                InvSys.sys.elems[i].img.sprite = img.sprite;
                InvSys.sys.elems[i].SetCount(count);
                InvSys.sys.elems[i].nameElem = name;
                InvSys.sys.elems[i].img.color = new Color(1, 1, 1, 1);
                gameObject.SetActive(false);
                return;
            }
        }

        winMess.SetActive(true);
    }
}
