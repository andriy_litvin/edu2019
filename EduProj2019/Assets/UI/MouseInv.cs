﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseInv : MonoBehaviour
{
    public static MouseInv mouse;
    public Image imgMouse;
    public Text countText;
    public int count;
    public string nameElem;
    public bool isMove;
    public ElemInv elem;

    private void Awake()
    {
        mouse = this;
    }

    void Start()
    {
        imgMouse = GetComponent<Image>();
        countText = GetComponentInChildren<Text>();
        imgMouse.color = new Color(1, 1, 1, 0);
        isMove = false;
        countText.text = "";
    }

    public void SetCount(int count)
    {
        this.count = count;
        countText.text = count == 0 ? "" : count.ToString();
    }

    void Update()
    {
        transform.position = Input.mousePosition;
    }
}
