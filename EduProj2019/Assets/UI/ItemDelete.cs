﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDelete : MonoBehaviour
{
    public GameObject winDel;
    public Text textMess;

    void Start()
    {
        
    }

    public void Drop()
    {
        if(MouseInv.mouse.imgMouse.sprite)
        {
            winDel.SetActive(true);
            textMess.text = "Delete " + MouseInv.mouse.nameElem + " (" + MouseInv.mouse.count + ")?";
            MouseInv.mouse.isMove = false;
        }
    }

    public void DelItem()
    {
        MouseInv.mouse.imgMouse.sprite = null;
        MouseInv.mouse.SetCount(0);
        MouseInv.mouse.nameElem = "";
        //MouseInv.mouse.isMove = false;
        MouseInv.mouse.imgMouse.color = new Color(1, 1, 1, 0);
        /*MouseInv.mouse.elem.img.sprite = null;
        MouseInv.mouse.elem.SetCount(0);
        MouseInv.mouse.elem.nameElem = "";
        MouseInv.mouse.elem.img.color = new Color(1, 1, 1, 0);*/
        winDel.SetActive(false);
    }

    public void NotDelete()
    {
        InvSys.sys.AddToInventar(MouseInv.mouse.imgMouse.sprite, MouseInv.mouse.count, MouseInv.mouse.nameElem);
        DelItem();
    }


}
