﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICntroler : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject settingsMenu;

    public void StartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void Update()
    {
        if(!settingsMenu.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))
        {
            mainMenu.SetActive(!mainMenu.activeInHierarchy);
        }
    }
}
