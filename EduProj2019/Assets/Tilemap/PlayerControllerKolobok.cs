﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerKolobok : MonoBehaviour
{
    public float speed = 1;
    public float cameraSpeed = 1;
    Rigidbody2D rb;
    Transform cam;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cam = Camera.main.transform;
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))*speed;
        cam.position = Vector3.MoveTowards(cam.position, transform.position, cameraSpeed);
        cam.position += Vector3.back;
    }
}
