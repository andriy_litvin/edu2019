﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TestGUI : MonoBehaviour
{
    public Rect rectGroup;
    string textFild1 = "", passwordFild1 = "", textArea1= "";
    public float hsVal, hsLeft=0, hsRight=10;
    public float hsbVal, hsbSize=1, hsbLeft=0, hsbRight=10;
    public Texture texture;
    bool tog1, tog2, tog3;
    bool panel;
    int idTool;
    string[] toolsBut = { "Tool1", "Tool2", "Tool3" };
    public Rect win1, win2;
    bool enadledWin1 = true;
    public GUISkin skin;

    private void OnGUI()
    {
        GUI.skin = skin;
        if (panel)
        {
            GUI.BeginGroup(rectGroup);
            GUI.Label(new Rect(10, 10, 200, 20), "My screen x = " + Screen.width + "; y = " + Screen.height);
            textFild1 = GUI.TextField(new Rect(10, 40, 200, 20), textFild1);
            passwordFild1 = GUI.PasswordField(new Rect(10, 70, 200, 20), passwordFild1, '*');
            textArea1 = GUI.TextArea(new Rect(10, 100, 200, 100), textArea1);
            GUI.EndGroup();
        }
        else
        {
            GUI.BeginGroup(new Rect(Screen.width - 210, Screen.height - 210, 200, 200));
            GUI.Box(new Rect(0, 0, 200, 200), "Menu");              //2               5    = 2/5=0.4
            hsVal = GUI.HorizontalSlider(new Rect(10, 30, 180, 20), hsVal, hsLeft, hsRight);
            hsbVal = GUI.HorizontalScrollbar(new Rect(10, 60, 180, 20), hsbVal, hsbSize, hsbLeft, hsbRight);
            GUI.DrawTexture(new Rect(10, 90, hsVal / hsRight * 180f, 20), texture);
            GUI.EndGroup();
        }

        tog1 = GUI.Toggle(new Rect(10, 270, 180, 20), tog1, "Toggle1");
        tog2 = GUI.Toggle(new Rect(10, 300, 180, 20), tog2, "Toggle2");
        tog3 = GUI.Toggle(new Rect(10, 330, 180, 20), tog3, "Toggle3");

        if (GUI.Button(new Rect(30, 360, 100, 30), "Click me"))
        {
            tog1 = false;
            tog2 = false;
            tog3 = false;
            panel = !panel;
        }

        idTool = GUI.Toolbar(new Rect(320, 20, 250, 30), idTool, toolsBut);
        if(idTool==0)
        {
            if (enadledWin1)
                win1 = GUI.Window(0, win1, PaintWindows, "Window 1");
        }
        else if(idTool==1)
        {
            enadledWin1 = true;
            win2 = GUI.Window(1, win2, PaintWindows, "Window 2");
        }
        else
        {
            enadledWin1 = true;
            GUI.Box(new Rect(320, 60, 250, 200), "Box 3");
        }
    }

    void PaintWindows(int id)
    {
        if (id == 0)
        {
            if (GUI.Button(new Rect(20, 40, 100, 30), "Close"))
            {
                enadledWin1 = false;
            }
        }
        else if(id==1)
        {
            textFild1 = GUI.TextField(new Rect(10, 40, 180, 20), textFild1);
        }
        GUI.DragWindow();
    }
}