﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenObjGame : MonoBehaviour
{
    public string objToFind;
    List<string> nameObj;

    void Start()
    {
        nameObj = new List<string>{"lopata", "listok", "almaz"};
       
         NextObjToFind();
    }

    public void NextObjToFind()
    {
        if(nameObj.Count>0)
        {
            int rand = Random.Range(0, nameObj.Count);
            print("find: " + nameObj[rand]);
            objToFind = nameObj[rand];
            nameObj.RemoveAt(rand);
        }
        else
        {
            print("GAME OVER!");
        }
    }

}
