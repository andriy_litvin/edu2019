﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockControl : MonoBehaviour
{
    void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag=="boolet")
        {
            GetComponent<Animator>().SetTrigger("isDestroy");
        }
        else if(collision.collider.tag=="Player")
        {
            if(collision.GetContact(0).normal == new Vector2(0,1))
            {
                GetComponent<Animator>().SetTrigger("isDestroy");
            }
        }

    }

    public void ObjDestroy()
    {
        Destroy(gameObject);
    }

}
