﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObj : MonoBehaviour
{
    public int idSpr;
    public Sprite[] sprites;
    public InitGame initG;
    float timer;
    //bool isFrize;

    // Start is called before the first frame update
    void Start()
    {
        SetRandomSprite();
        timer = Random.Range(2f, 5f);
    }

    // клік мишею
    void OnMouseDown()
    {
        if (idSpr == 0)
            initG.score++;
        else
            initG.score -= 2;
        print("score: " + initG.score);
        Destroy(gameObject); // знищення поточного об'єкта (на який натиснули)
    }

    private void Update()
    {
        //if (!isFrize)
        //{
            timer -= Time.deltaTime;

            if (timer < 0)
            {
                // призначення нового спрайта
                SetRandomSprite();

                transform.position = new Vector3(Random.Range(initG.marker1.position.x, initG.marker2.position.x),
                        Random.Range(initG.marker1.position.y, initG.marker2.position.y), 0);
                timer = Random.Range(2f, 5f);
            }
        //}
    }

    void SetRandomSprite()
    {
        // вибираємо id спрайта з масиву 
        idSpr = Random.Range(0, sprites.Length);
        // встановлюємо відповідний спрайт для відображення в SpriteRenderer
        GetComponent<SpriteRenderer>().sprite = sprites[idSpr];
    }
}
