﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet3d : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag=="nps")
        {
            collision.collider.GetComponent<NPC3>().Damage(Random.Range(5, 10));
        }

        Destroy(gameObject);
    }
}
