﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpherePatrul : MonoBehaviour
{
    public float speed=0.1f;
    public Transform[] markers;
    int idNext=1;

    void Start()
    {
        
    }

    void Update()
    {
        if(Vector3.Distance(transform.position, markers[idNext].position)>0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, markers[idNext].position, speed*Time.deltaTime);
        }
        else
        {
            idNext++;
            if (idNext == markers.Length)
                idNext = 0;

        }
    }
}
