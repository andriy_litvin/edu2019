﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SokyraNPC : MonoBehaviour
{
    PoolObj poolObj;

    private void Start()
    {
        poolObj = GetComponent<PoolObj>();
    }

    private void OnBecameInvisible()
    {
        //Destroy(gameObject);
        poolObj.DestroyObj();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag!="Player")
        {
            //Destroy(gameObject,0.5f);
            poolObj.DestroyObj();
        }
        else
        {
            //Destroy(gameObject);
            poolObj.DestroyObj();
            collision.collider.GetComponent<PlayerCat>().Death();
        }
    }

}
