﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestKorutin : MonoBehaviour
{
    Coroutine cor;

    void Start()
    {
        cor = null;
        // викликаємо метод "PrintTestMess" через 2 секунди
        Invoke("PrintTestMess", 2f);

        // виклик корутини
        //StartCoroutine(TimeTick());
        //StartCoroutine(TestCor());
    }

    void PrintTestMess()
    {
        print("PrintTestMess");
    }

    void Update()
    {

        if(Input.GetKeyDown(KeyCode.Z) && cor==null)
        {
            cor = StartCoroutine(RotateCor());
        }
        if (Input.GetKeyDown(KeyCode.X) && cor!=null)
        {
            StopCoroutine(cor);
            cor = null;
            //StopAllCoroutines();
        }
    }

    IEnumerator TimeTick()
    {
        print("start");
        yield return new WaitForSeconds(3);  // затримка на 3 секунди
        print("tick");
        yield return new WaitForSeconds(1);
        print("tick1");
        yield return new WaitForSeconds(2);
        print("stop");
    }

    IEnumerator TestCor()
    {
        while (true) 
        {
            yield return null;  // затримка на 1 кадр
            print(Time.deltaTime);
        }
    }

    IEnumerator RotateCor()
    {
        while(true)
        {
            yield return null;
            transform.Rotate(0, 0, 40*Time.deltaTime); // повертаємо об'єкт на 5 градусів за кадр по осі z
        }
    }



}
