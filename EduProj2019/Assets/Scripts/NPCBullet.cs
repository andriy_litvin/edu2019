﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBullet : MonoBehaviour
{
    public Transform target;
    public float speed = 5f;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(target)
        {
            transform.LookAt(target);
            rb.velocity = transform.TransformDirection(0, 0, speed); 
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag=="Player")
        {
            PlayerController3d.player.Damage(10);
        }
        Destroy(gameObject);
    }
}
