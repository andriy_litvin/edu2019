﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCameraRay : MonoBehaviour
{
    public Camera cam1;
    public Camera cam2;
    Vector4 rect1;
    Vector4 rect0;
    Vector4 rectC;

    // Start is called before the first frame update
    void Start()
    {
        rect1 = new Vector4(0, 0, 1, 1);
        rect0 = new Vector4(0.02f, 0.03f, 0.2f, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            StartCoroutine(AnimCamera(cam1, cam2));
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StartCoroutine(AnimCamera(cam2, cam1));
        }
    }

    IEnumerator AnimCamera(Camera camMax, Camera camMin)
    {
        camMax.depth = 1;
        camMin.depth = 0;

        while(true)
        {
            yield return null;
            rectC = new Vector4(camMax.rect.x, camMax.rect.y, camMax.rect.width, camMax.rect.height);
            rectC = Vector4.MoveTowards(rectC, rect1, 0.05f);
            camMax.rect = new Rect(rectC.x, rectC.y, rectC.z, rectC.w);

            if(Vector4.Distance(rectC, rect1)<0.001f)
            {
                break;
            }
        }

        while (true)
        {
            yield return null;
            rectC = new Vector4(camMin.rect.x, camMin.rect.y, camMin.rect.width, camMin.rect.height);
            rectC = Vector4.MoveTowards(rectC, rect0, 0.05f);
            camMin.rect = new Rect(rectC.x, rectC.y, rectC.z, rectC.w);

            if (Vector4.Distance(rectC, rect0) < 0.001f)
            {
                break;
            }
        }
        camMax.depth = 0;
        camMin.depth = 1;
    }
}
