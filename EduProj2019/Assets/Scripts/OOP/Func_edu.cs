﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Func_edu : MonoBehaviour
{
    void Start()
    {
        //print("suma = "+ Suma(4, 4));

        /*
        int[] mas = new int[7];
        ArrayRandom(mas, 1, 10);
        ArrayPrint(mas);
        print("max = " + ArrayMax(mas));
        */
        /*
        PrintInfo("Pasha", 20);
        PrintInfo(age:20, youName:"Sasha");
        PrintInfo("Den");
        PrintInfo(age:22);
        */
        /*
        SumaPrint(5, 9);
        SumaPrint(2.5f);
        SumaPrint(1.2f, 5.8f);
        */

        /* 
        // out       
        int a;
        AddNumber(out a);
        print("a = " + a);
        */
        /*
        // ref      
        int a=5;
        AddNumber(ref a);
        print("a = " + a);
        */
        /*
        print("5= "+ Suma(3,5,7,8,9));
        print("0= "+Suma());
        print("3= "+Suma(3, 5, 1));
        */

        //print("factorial = " + Factorial(3));

        print("8a = " + ElementProg(5, 1, 10));
        print("8b = " + SumaElemsProg(4, 5, 2));
    }

    // 5 7 9 11 = 32

    int SumaElemsProg(int n, int a, int d)
    {
        if(n==1)
        {
            return a;
        }
        else
        {
            return SumaElemsProg(n - 1, a, d) + a + d * (n - 1);
        }
    }

    int ElementProg(int n, int a, int d)
    {
        if(n==1)
        {
            return a;
        }
        else
        {
            return ElementProg(n - 1, a, d)+d; 
        }
    }

    int Factorial(int x)
    {
        print(x);
        return x == 1 ? 1 : Factorial(x - 1)*x;
    }


    // int Suma(params int[] nums, params int[] nums2) - error
    // int Suma(params int[] nums, int a, float b) - error
    // int Suma(int a, float b, params int[] nums) - ok

    int Suma(params int[] nums)
    {
        int sum = 0;

        for(int i=0; i<nums.Length; i++)
        {
            sum += nums[i];
        }

        return sum;
    }

    /*
    void AddNumber(out int num) 
    {
        num = 4;
        num++;
        print("add = " + num);
    }
    */


    void AddNumber(ref int num)
    {
        num++;
        print("add = " + num);
    }


    void PrintInfo(string youName="You", int age=1)
    {
        print(youName + " " + age + " years");
    }


    int ArrayMax(int[] array)
    {
        int max = -1000;
        for (int i = 0; i < array.Length; i++)
        {
            if(max<array[i])
            {
                max = array[i];
            }
        }
        return max;
    }

    void ArrayRandom(int[] array, int min, int max)
    {
        for(int i=0; i<array.Length; i++)
        {
            array[i] = Random.Range(min, max);
        }
    }

    void ArrayPrint(int[] array)
    {
        string str = "";

        for(int i=0; i<array.Length; i++)
        {
            str += array[i] + " ";
        }
        print(str);
    }

    void SumaPrint(int num1, int num2)
    {
        Debug.LogFormat("{0} + {1} = {2}", num1, num2, num1 + num2);
    }

    /*void SumaPrint(int num1 )
    {
        Debug.LogFormat("{0} + 1 = {1}", num1, num1++);
    }*/

    int SumaPrint(int num1)
    {
        return num1++;
    }

    void SumaPrint(float num1, float num2)
    {
        Debug.LogFormat("{0} + {1} = {2}", num1, num2, num1 + num2);
    }

    void SumaPrint(float num1)
    {
        Debug.LogFormat("{0} + 1 = {1}", num1, num1+1);
    }

    int Suma(int num1, int num2)
    {
        return num1 + num2;
    }

}
