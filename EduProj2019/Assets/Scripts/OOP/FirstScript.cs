﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        print("Hello \nUnity"); 
        Debug.Log("World");
        Debug.LogWarning("warning");
        Debug.LogError("error");

    }

    void Update()
    {
        print("Hello Unity");
    }

}
