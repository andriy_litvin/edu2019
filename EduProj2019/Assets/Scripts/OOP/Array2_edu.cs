﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Array2_edu : MonoBehaviour
{
    const int rowsMas = 15;
    const int colsMas = 10;

    void Start()
    {
        int[,] mas = new int[rowsMas, colsMas]; // new int[10, 5];
        int[,] mas2 = { { 2, 3, 4, 5 }, 
                        { 2, 3, 5, 7 } }; // 2x4

        mas[1, 2] = 4;
        /*
        for(int i=0; i<rowsMas; i++) // обхід рядків масиву
        {
            for(int j=0; j<colsMas; j++) // обхід елементів в рядку масива
            {
                mas[i, j] = Random.Range(10,20);
            }
        }

        string str = "";
        for (int i = 0; i < rowsMas; i++) // обхід рядків масиву
        {
            for (int j = 0; j < colsMas; j++) // обхід елементів в рядку масива
            {
                str += mas[i, j] + " ";
            }
            str += "\n";
        }
        print(str);
        */

        // заповнити масив величиною 7х7 так щоб по діагоналях були одиниці, а 
        // всі інші елементи нулі
        /*
        int[,] array1 = new int[7, 7];

        for(int i=0; i<7; i++)
        {
            array1[i, i] = 1;
            array1[i, 6 - i] = 1;
        }

        string str = "";
        for (int i = 0; i < 7; i++) // обхід рядків масиву
        {
            for (int j = 0; j < 7; j++) // обхід елементів в рядку масива
            {
                str += array1[i, j] + " ";
            }
            str += "\n";
        }
        print(str);

        // заповнити масив величиною 7х7 послідовними значеннями 
        int num = 1;
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                array1[i, j] = num;
                num++;
            }
        }

        str = "";
        for (int i = 0; i < 7; i++) // обхід рядків масиву
        {
            for (int j = 0; j < 7; j++) // обхід елементів в рядку масива
            {
                str += array1[i, j] + " ";
            }
            str += "\n";
        }
        print(str);
        */
        /*
        int[,] array2 = new int[6, 6];
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                array2[i, j] = ((i+j) % 6)+1;
            }
        }

        string str = "";
        for (int i = 0; i < 6; i++) // обхід рядків масиву
        {
            for (int j = 0; j < 6; j++) // обхід елементів в рядку масива
            {
                str += array2[i, j] + " ";
            }
            str += "\n";
        }
        print(str);
        */
        /*
        int[,] array3 = new int[12, 10];
        int num = 1;

        for (int i = 0; i < 10; i++) // обхід по колонкам масиву
        {
            for (int j = 11; j >=0 ; j--) // обхід елементів колонки з низу в гору
            {
                array3[j,i] = num;
                num++;
            }
        }

        string str = "";
        for (int i = 0; i < 12; i++) // обхід рядків масиву
        {
            for (int j = 0; j < 10; j++) // обхід елементів в рядку масива
            {
                str += array3[i, j] + " ";
            }
            str += "\n";
        }
        print(str);
        */

        // дано двохвимірний масив, знайти суму елементів
        // в кожному рядку та колонці окремо

        int[,] array = new int[5, 5]; // вхідний масив
        int[] sumaRows = new int[5]; // сума по рядкам
        int[] sumaCols = new int[5]; // сума по колонкам

        // заповнимо масив
        for (int i = 0; i < 5; i++) // обхід рядків масиву
        {
            for (int j = 0; j < 5; j++) // обхід елементів в рядку масива
            {
                array[i, j] = Random.Range(10, 20);
            }
        }

        // рахуємо суму по рядкам
        for (int i = 0; i < 5; i++) 
        {
            for (int j = 0; j < 5; j++) 
            {
                sumaRows[i] += array[i, j];
            }
        }

        // рахуємо суму по колонках
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                sumaCols[i] += array[j,i];
            }
        }


        string str = "";
        for (int i = 0; i < 5; i++) // обхід рядків масиву
        {
            for (int j = 0; j < 5; j++) // обхід елементів в рядку масива
            {
                str += array[i, j] + " ";
            }
            str += " | "+ sumaRows[i] +"\n";
        }
        str += "--------------------\n";
        for(int i=0; i<5; i++)
        {
            str += sumaCols[i] + " ";
        }
        print(str);


        //-----------------------------------
        // масиви масивів (ступінчасті масиви)

        int[][] array5 = new int[5][];
        int[] array51 = { 6, 3, 4, 2, 5 };

        for(int i=0; i<5; i++)
        {
            array5[i] = new int[array51[i]];
        }

        for (int i = 0; i < 5; i++)
        {
            for (int j=0; j<array51[i]; j++)
            {
                array5[i][j] = Random.Range(1, 10);
            }
        }

        str = "";
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < array51[i]; j++)
            {
                str+= array5[i][j] +" ";
            }
            str += "\n";
        }
        print(str);



    }


}
