﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCls_edu : MonoBehaviour
{
    void Start()
    {
        /*Piple person = new Piple();
        Piple person2 = new Piple("Tolik", 50, 150, 1990, 'M', Piple.Sex.Men);
        print(person.name);
        print(person2.name);

        person.PrintInfo();
        person2.PrintInfo();

        person.Dialog("Hello");
        person2.Dialog("Hello");
        person.Dialog("...");
        person2.Dialog("By");
        */

        /*print(person.age);
        person.age = 10;
        print(person.age);
        person.age = -10;
        print(person.age);


        print(person.sex);
        person.sex = 'M';
        print(person.sex);
        person.sex = 'S';
        print(person.sex);

        person.pipleSex = Piple.Sex.Men;*/

        /*
        TellCost tell = new TellCost(5, 0.5f);
        print(tell.Cost());

        tell.first = 15;
        print(tell.Cost());
        */

        /*Complex com = new Complex(2, 4, 3, 2);

        ComplexNum comNum = com.Add();

        print(comNum.toString());

        print(Propys(12));
        print(Propys(48));
        print(Propys(722));
        print(Propys(270));*/

        Date date = new Date("2019.05.10");
        date.MinusDaysToDate(1000);
        date.Display();
        //print("days = "+date.DayMijDates(2019,7,22));
       
    }


    string Propys(int a) 
    {
        string str = "";

        if(a==0)
        {
            return "нуль";
        }

        int n1 = a % 100;

        if (n1 <= 19 && n1>=10)
        {
            switch (n1)
            {
                case 10:
                    str += "десять";
                    break;
                case 11:
                    str += "одинадцять";
                    break;
                case 12:
                    str += "дванадцять";
                    break;
                case 13:
                    str += "тринадцять";
                    break;
                case 14:
                    str += "чотирнадцять";
                    break;
                case 15:
                    str += "п'ятнадцять";
                    break;
                case 16:
                    str += "шістнадцять";
                    break;
                case 17:
                    str += "сімнадцять";
                    break;
                case 18:
                    str += "вісімнадцять";
                    break;
                case 19:
                    str += "девятнадцять";
                    break;
            }
        }
        else 
        {
            switch (n1%10)
            {
                case 1:
                    str += "один";
                    break;
                case 2:
                    str += "два";
                    break;
                case 3:
                    str += "три";
                    break;
                case 4:
                    str += "чотири";
                    break;
                case 5:
                    str += "п'ять";
                    break;
                case 6:
                    str += "шість";
                    break;
                case 7:
                    str += "сім";
                    break;
                case 8:
                    str += "вісім";
                    break;
                case 9:
                    str += "девять";
                    break;
            }
        }

        int n2 = (n1 / 10)*10;
        switch(n2)
        {
            case 20:
                str = "двадцять "+str;
                break;
            case 30:
                str = "тридцять " + str;
                break;
            case 40:
                str = "сорок " + str;
                break;
            case 50:
                str = "п'ятдесят " + str;
                break;
            case 60:
                str = "шістдесят " + str;
                break;
            case 70:
                str = "сімдесят " + str;
                break;
            case 80:
                str = "вісімдесят " + str;
                break;
            case 90:
                str = "девяносто " + str;
                break;
        }

        // 12321    123     3   300
        int n3 = ((a / 100)%10)*100;

        switch (n3)
        {
            case 100:
                str = "сто " + str;
                break;
            case 200:
                str = "двісті " + str;
                break;
            case 300:
                str = "триста " + str;
                break;
            case 400:
                str = "чотириста " + str;
                break;
            case 500:
                str = "п'ятсот " + str;
                break;
            case 600:
                str = "шістсот " + str;
                break;
            case 700:
                str = "сімсот " + str;
                break;
            case 800:
                str = "вісімсот " + str;
                break;
            case 900:
                str = "девятсот " + str;
                break;
        }

        return str;
    }


}
