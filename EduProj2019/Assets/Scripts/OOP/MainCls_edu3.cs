﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCls_edu3 : MonoBehaviour
{
    void Start()
    {
        /*List<Person> persons = new List<Person>();
        persons.Add(new Person("Vitalik", 20));
        persons.Add(new Student("Tolik",18,"RDGU","IT"));
        persons.Add(new Pupil("Djim", 12, "#5", 6));

        foreach(Person p in persons)
        {
            print( p.Info());
        }*/

        /*List<Animal1> animals = new List<Animal1>();
        animals.Add(new Parrot("Kesha", true));
        animals.Add(new Cat1("Barsik", false));

        foreach(Animal1 a in animals)
        {
            a.Info();
        }*/

        CabelMod cabel = new CabelMod("RB", 3, 0.8f, true);
        cabel.Info();
        print("quality: "+ cabel.Quality());

        Men[] mens = new Men[7];
        mens[0] = new Novachok("Petro", 35, 5);
        mens[1] = new Opytnyj("Vasja", 40, 7);
        mens[2] = new Veteran("Hosha", 50, 15);
        mens[3] = new Opytnyj("Ivan", 23, 10);
        mens[4] = new Novachok("Ira", 50, 10);
        mens[5] = new Opytnyj("Andriy", 30, 8);
        mens[6] = new Veteran("Dima", 47, 17);

        foreach(Men m in mens)
        {
            if(m.Shoot())
            {
                print("popav");
                m.Info();
                break;
            }
            else
            {
                print("ne popav");
                m.Info();
            }
        }


    }
}






