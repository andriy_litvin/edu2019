﻿using UnityEngine;

class Person
{
    public string name;
    public int age;


    public Person(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    // Метод який може бути перевизначений в дочірніх класах
    public virtual string Info()
    {
        return "Person: " + name + ", " + age + "y.";
    }
}


class Student:Person
{
    public string NameHighSchool;
    public string Specialization;

    public Student(string name, int age, string NameHighSchool, string Specialization):base(name,age)
    {
        this.NameHighSchool = NameHighSchool;
        this.Specialization = Specialization;
    }

    public override string Info()
    {
        return base.Info() + "\nStudent: " + NameHighSchool + ", " + Specialization;
    }
}

class Pupil : Person
{
    public string nameSchool;
    public int classe;

    public Pupil(string name, int age, string nameSchool, int classe) : base(name, age)
    {
        this.nameSchool = nameSchool;
        this.classe = classe;
    }

    public override string Info()
    {
        return base.Info() + "\nPupil: " + nameSchool + ", " + classe;
    }

}




abstract class Animal1
{
    public string name;
    public string type;

    public abstract void Info();
}

class Parrot:Animal1
{
    public bool isVois;

    public Parrot(string name, bool isVois)
    {
        this.name = name;
        this.isVois = isVois;
        type = "Bird";
    }

    public override void Info()
    {
        Debug.LogFormat("Parrot: {0}, {1}, vois: {2}", name, type, isVois);
    }

}

class Cat1: Animal1
{
    public bool isGames;

    public Cat1(string name, bool isGames)
    {
        this.name = name;
        this.isGames = isGames;
        type = "Savec";
    }

    public override void Info()
    {
        Debug.LogFormat("Cat: {0}, {1}, games: {2}", name, type, isGames);
    }

}



class Cabel
{
    public string type;
    public int countZHYL;
    public float diametr;

    public Cabel(string type, int countZHYL, float diametr)
    {
        this.type = type;
        this.countZHYL = countZHYL;
        this.diametr = diametr;
    }

    public virtual void Info()
    {
        Debug.LogFormat("type: {0}, countZHYL: {1}, diametr: {2}", type, countZHYL, diametr);
    }

    public virtual float Quality()
    {
        return diametr / countZHYL;
    }
}

class CabelMod: Cabel
{
    public bool isOpletka;

    public CabelMod(string type, int countZHYL, float diametr, bool isOpletka):base(type, countZHYL,diametr)
    {
        this.isOpletka = isOpletka;
    }

    public override void Info()
    {
        base.Info();
        Debug.Log("Opletka: " + isOpletka);
    }

    public override float Quality()
    {
        if (isOpletka)
            return 2 * base.Quality();
        else
            return 0.7f * base.Quality();
    }

}


abstract class Men
{
    public string name;
    public int age;
    public int staj;

    public abstract bool Shoot();

    public void Info()
    {
        Debug.LogFormat("Name: {0}, age: {1}, staj: {2}",name, age, staj);
    }
}

class Novachok: Men
{
    public Novachok(string name, int age, int staj)
    {
        this.name = name;
        this.age = age;
        this.staj = staj;
    }

    public override bool Shoot()
    {
        float skill = 0.01f * staj;
        if (Random.Range(0f, 1f) <= skill)
            return true;
        else
            return false;
    }
}

class Opytnyj:Men
{
    public Opytnyj(string name, int age, int staj)
    {
        this.name = name;
        this.age = age;
        this.staj = staj;
    }

    public override bool Shoot()
    {
        float skill = 0.05f * staj;
        if (Random.Range(0f, 1f) <= skill)
            return true;
        else
            return false;
    }
}

class Veteran:Men
{
    public Veteran(string name, int age, int staj)
    {
        this.name = name;
        this.age = age;
        this.staj = staj;
    }

    public override bool Shoot()
    {
        float skill = 0.9f - 0.01f*age;
        if (Random.Range(0f, 1f) <= skill)
            return true;
        else
            return false;
    }
}