﻿using UnityEngine;
using System;

class Piple
{
    public string name;
    public int masa;
    public int rist;

    private int _yearBirth;
    private char _sex; // M W
    public Sex pipleSex;

    public string kolirOchey { get; }

    public int yearBirth
    {
        get { return _yearBirth; }
        set { 
            if(value<1900)
            {
                _yearBirth = 1900;
            }
            else
            {
                _yearBirth = value;
            }
        }
    }

    public int age
    {
        get {
            DateTime date = DateTime.Today.Date;
            DateTime date2 = new DateTime(_yearBirth, 1, 1).Date;
            TimeSpan d = date - date2;

            return d.Days/365; 
        }
    }

    public char sex
    {
        get { return _sex;  }
        set { 
            if(value == 'M' || value == 'W') 
            {
                _sex = value; 
            }
            else
            {
                Debug.LogError("Nevidomuy sumvol '" + value + "' vvedit 'M' abo 'W'");
            }
        }
    }

    public enum Sex
    {
        Men,
        Women
    }

    public Piple()
    {
        name = "no name";
        masa = 0;
        rist = 0;
        _yearBirth = 1900;
        _sex = 'M';
        pipleSex = Sex.Men;
    }

    public Piple(string name, int masa, int rist, int yearBirth, char sex, Sex sexPipl)
    {
        this.name = name;
        this.masa = masa;
        this.rist = rist;
        this.yearBirth = yearBirth;
        this.sex = sex;
        pipleSex = sexPipl;
    }

    ~Piple()
    {
        Debug.Log("pa-pa "+name);
    }


    public void PrintInfo()
    {
        Debug.LogFormat("name = {0}, masa = {1}, rist = {2}, yearBirth = {3}, sex = {4}, sexPipl = {5}, age = {6}",
                        name, masa, rist, yearBirth, sex, pipleSex, age);
    }

    public void Dialog(string mes)
    {
        Debug.LogFormat("{0}: {1}", name, mes);
    }

}


class TellCost
{
    public int first;  // час
    public float second;  // вармість за 1 хв.

    public TellCost()
    {
        first = 0;
        second = 0;
    }

    public TellCost(int first, float second)
    {
        this.first = first;
        this.second = second;
    }

    public void Display()
    {
        Debug.LogFormat("fistr = {0}, second = {1}", first, second);
    }

    public float Cost()
    {
        return first * second;
    }

}


class Complex
{
    public ComplexNum com1;
    public ComplexNum com2;

    public Complex()
    {
        com1.a = 0;
        com1.b = 0;
        com2.a = 0;
        com2.b = 0;

    }

    public Complex(float com1a, float com1b, float com2a, float com2b)
    {
        com1.a = com1a;
        com1.b = com1b;
        com2.a = com2a;
        com2.b = com2b;
    }

    public string toString()
    {
        return "("+com1.a+":"+com1.b+")";//....
    }

    public ComplexNum Add()
    {
        ComplexNum comRezul;
        comRezul.a = com1.a + com2.a;
        comRezul.b = com1.b + com2.b;
        return comRezul; 
    }




}

struct ComplexNum
{
    public float a;
    public float b;

    public string toString()
    {
        return "(" + a + ":" + b + ")";
    }
}

class Date
{
    public uint year;
    private uint _month;
    private uint _day;

    public uint month
    {
        get { return _month; }
        set
        {
            if(value>=1 && value <=12)
            {
                _month = value;
            }
            else
            {
                Debug.LogFormat("Misac '{0}' ne isnuje", value);
                if (_month == 0)
                    _month = 1;
                    
            }
        }
    }

    public uint day
    {
        get { return _day; }
        set 
        {
            if (value >= 1 && value <= DaysInMonth(_month))
                _day = value;
            else
            {
                if (_day == 0)
                    _day = 1;
                Debug.LogFormat("Day '{0}' v {1} misaci ne isnuje", value, _month);
            }
        }
    }

    public Date(uint year, uint month, uint day)
    {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public Date(string date)
    {
        string[] dateS = date.Split('.');
        if(dateS.Length==3)
        { 
            year = uint.Parse(dateS[0]);
            month = uint.Parse(dateS[1]);
            day = uint.Parse(dateS[2]);
        }
        else 
        {
            Debug.Log("error date");
            year = 1900;
            month = 1;
            day = 1;
        }
    }

    public void Display()
    {
        Debug.LogFormat("{0}.{1}.{2}",year, month, day);
    }

    public bool IsVusokosnuyRik(uint year)
    {
        // перевіряємо чи рік кратний 4
        if (year % 4 == 0)
        {
            // перевіряємо на можливі винятки 
            if (year % 100 == 0 && year % 400 == 0)
            {
                // виводить роки, що є кратні 100 і 400 
                return true;
            }
            else if (year % 100 != 0)
            {
                // виводимо роки, ко є не кратні 100 але кратні 4
                return true;
            }
            else
            {
                // виводимо роки, що не кратні 400 
                return false;
            }
        }
        else // рік не кратний 4 
        {
            return false;
        }

    }

    // 2019-5-10 +30d -> 2019-6-9

    public void AddDaysToDate(uint days)
    {
        uint dayDelta = 0;

        do
        {
            if (day == DaysInMonth(month))
            {
                if (month == 12)
                {
                    month = 1;
                    year++;
                }
                else
                {
                    month++;
                }
                day = 1;
                days--;
                //Debug.LogFormat("3 dayDelta={0}, days={1}, day={2}", dayDelta, days, day);
            }
            else
            {
                dayDelta = DaysInMonth(month) - day;
                if (dayDelta > 0)
                {
                    if (dayDelta <= days)
                    {
                        day += dayDelta;
                        days -= dayDelta;
                        //Debug.LogFormat("1 dayDelta={0}, days={1}, day={2}", dayDelta, days, day);
                    }
                    else if (dayDelta > days)
                    {
                        day += days;
                        days = 0;
                        //Debug.LogFormat("2 dayDelta={0}, days={1}, day={2}", dayDelta, days, day);
                    }
                }
            }
        } while (days>0);

    }

    public uint DaysInMonth(uint month)
    {
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                {
                    if (IsVusokosnuyRik(year))
                    {
                        return 29;
                    }
                    else
                    {
                        return 28;
                    }
                }
        }
        return 0;
    }

    public int DayMijDates(int year, int month, int day)
    {
        DateTime date1 = new DateTime((int)this.year, (int) this.month,(int)this.day);
        DateTime data2 = new DateTime(year, month, day);
        var delta = date1 - data2;
        return delta.Days;
    }

    public void MinusDaysToDate(uint days)
    {    
        do
        {
            if (day == 1)
            {
                if (month == 1)
                {
                    month = 12;
                    year--;
                }
                else
                {
                    month--;
                }
                day = DaysInMonth(month);
                days--;
                //Debug.LogFormat("3 dayDelta={0}, days={1}, day={2}", dayDelta, days, day);
            }
            else
            {         
                if (days >= day)
                {                          
                    days -= day-1;
                    day = 1;
                    //Debug.LogFormat("1 dayDelta={0}, days={1}, day={2}", dayDelta, days, day);
                }
                else 
                {
                    day -= days;
                    days = 0;
                    //Debug.LogFormat("2 dayDelta={0}, days={1}, day={2}", dayDelta, days, day);
                }                
            }
        } while (days > 0);
    }         
}

