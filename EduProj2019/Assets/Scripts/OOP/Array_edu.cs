﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Array_edu : MonoBehaviour
{
    public string[] masStr;

    void Start()
    {
        int[] mas = new int[8];
        float[] mas2 = new float[4] { 2.3f, 3.5f, 5.8f, 3.4f};
        float[] mas3 = { 2.3f, 3.5f, 5.8f, 3.4f };

        int masLen = mas.Length;

        print(mas3[1]);
        mas3[1] = 5.5f;
        print(mas3[1]);

        string str = "";
        for (int i=0; i<mas.Length; i++)
        {
            str += mas[i] + " ";
        }
        print(str);


        for (int i = 0; i < mas.Length; i++)
        {
            mas[i] = Random.Range(1, 10);
        }

        str = "";
        for (int i = 0; i < mas.Length; i++)
        {
            str += mas[i] + " ";
        }
        print(str);
        /*
        // заповнити масив -> 0 1 2 3 4 5 6 7

        for (int i = 0; i < mas.Length; i++)
        {
            mas[i] = i;
        }

        str = "";
        for (int i = 0; i < mas.Length; i++)
        {
            str += mas[i] + " ";
        }
        print(str);

        // заповнити масив -> 7 6 5 4 3 2 1 0 
        for (int i = 0; i < mas.Length; i++)
        {
            mas[i] = mas.Length - 1 - i;  // 8 - 1 - i
        }

        str = "";
        for (int i = 0; i < mas.Length; i++)
        {
            str += mas[i] + " ";
        }
        print(str);
        */
        /*
        // сума елементів масиву
        int suma = 0;
        for (int i = 0; i < mas.Length; i++)
        {
            suma += i;
        }
        print("suma = "+ suma);

        // кількість парних та не парних
        int countPar = 0;
        int countNePar = 0;
        foreach (var item in mas)
        {
            if(item % 2 == 0)
            {
                countPar++;
            }
        }

        countNePar = mas.Length - countPar;
        print("parinyh = " + countPar + "; ne parnyh = " + countNePar);
        Debug.LogFormat("parinyh = {0}; ne parnyh = {1}", countPar, countNePar);
        */
        /*
        // замінити парний на 2 та не парний на 1
        for (int i = 0; i < mas.Length; i++)
        {
            if (mas[i] % 2 == 0)
            {
                mas[i] = 2;
            }
            else
            {
                mas[i] = 1;
            }
        }

        str = "";
        for (int i = 0; i < mas.Length; i++)
        {
            str += mas[i] + " ";
        }
        print(str);

        // знайти номер елемента масиву за його значенням. Якщо такого елемента в масиві нема повернути -1
        int pos = -1; // позиція
        int num = 2; // шуканий елемента

        for (int i = 0; i < mas.Length; i++)
        {
            if(mas[i]==num)
            {
                pos = i;
                break;
            }
        }
        print("pos = " + pos);
        */
        /*
        // в масиві знайти найбільший елемент та його позицію. Вивисти кількість найбільших елементів
        int max = -10; // значення максимального елем.
        int pos = -1; // позиція максимального елем.

        // пошук максимального
        for (int i = 0; i < mas.Length; i++)
        {
            if(max<mas[i])
            {
                max = mas[i];
                pos = i;
            }
        }
        Debug.LogFormat("MAX = {0} v pos = {1}", max, pos);

        // кількість максимальних елем.
        int countMax = 0;
        foreach (var item in mas)
        {
            if(item==max)
            {
                countMax++;
            }
        }
        print("max v mas = "+ countMax);
        */

        int min = 1000;
        int posMin = -1;
        int temp = 0;

        for(int i=0; i<mas.Length-1; i++)
        {
            for(int j=i; j<mas.Length; j++)
            {
                // пошук найменшого значення з невідсортованої частини масиву
                if (min > mas[j]) 
                {
                    min = mas[j]; // зберігаємо нове мін. знач.
                    posMin = j; // зберігаємо позицію в масиві нового мін. знач.
                }
            }
            // переміщаємо наступне значення на своє місце в відсортованому масиві
            temp = mas[i];
            mas[i] = mas[posMin];
            mas[posMin] = temp;
            min = 1000;
        }

        str = "";
        for (int i = 0; i < mas.Length; i++)
        {
            str += mas[i] + " ";
        }
        print(str);

    }
}