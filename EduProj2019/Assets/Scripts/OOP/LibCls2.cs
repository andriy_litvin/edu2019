﻿using System;
using UnityEngine;

class Animal
{
    public string name;
    protected int a;

    public Animal(string name)
    {
        this.name = name;
        Debug.Log("Animal");
    }

    public void Info()
    {
        Debug.LogFormat("name = {0}, a = {1}", name, a);
    }

}

class Dog:Animal
{
    public int countAtack;

    public Dog(string name, int countAtack): base(name)
    {
        this.countAtack = countAtack;
        Debug.Log("Dog");
    }

    public void Gav()
    {
        int countGav = UnityEngine.Random.Range(1, 4);

        for(int i=0; i<countGav; i++)
        {
            Debug.Log("gav");
        }
    }

    public void Guard()
    {
        Debug.Log("Guard"+a);
    }
}

class Cat: Animal
{
    public int countMouse;

    public Cat(string name, int countMouse): base(name)
    {
        this.countMouse = countMouse;
        Debug.Log("Cat");
    }

    public void Mjau()
    {
        int countMjau = UnityEngine.Random.Range(1, 3);

        for (int i = 0; i < countMjau; i++)
        {
            Debug.Log("mjau");
        }
    }

    public void CatchMouse()
    {
        Debug.Log("catch mouse");
    }
}

class Taxa: Dog 
{ 
    public Taxa(string name, int countAtack): base(name, countAtack)
    {
        Debug.Log("Taxa");
    }

    public void Print()
    {
        Debug.Log(a);
    }
}

class Calorii
{
    public float calorii100g;
    public float masaGram;

    public Calorii(float calorii100g, float masaGram)
    {
        this.calorii100g = calorii100g;
        this.masaGram = masaGram;
    }

    public string Info()
    {
        return "calorii na 100g = " + calorii100g + ", masa = " + masaGram + " g";
    }

    public float CalorProd()
    {
        return calorii100g / 100f * masaGram;
    }
}

class VitaminC:Calorii
{
    public float vitaminC_1g;

    public VitaminC(float calorii100g, float masaGram, float vitaminC_1g): base(calorii100g, masaGram)
    {
        this.vitaminC_1g = vitaminC_1g;
    }

    public float VitaminCProdukt()
    {
        return vitaminC_1g * masaGram;
    }
}

class Time2
{
    public int hour;
    public int minute;
    public int second;

    public Time2()
    {
        hour = DateTime.Now.Hour;
        minute = DateTime.Now.Minute;
        second = DateTime.Now.Second;
    }

    public Time2(int hour, int minute, int second)
    {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int CountMinute24_00()
    {
        int deltaHour = 24 - hour;
        if(minute==0)
        {
            return deltaHour * 60;
        }
        else
        {
            return minute + (deltaHour - 1) * 60;
        }
    }

    public void Add100m()
    {
        DateTime time = new DateTime(1, 1, 1, hour, minute, second);
        time = time.AddMinutes(100);
        hour = time.Hour;
        minute = time.Minute;
        second = time.Second;
    }

    public string InfoTime()
    {
        return "("+ hour + ":"+ minute + ":"+ second + ")";
    }

}


class Rozklad: Time2
{
    public string disciplina;
    public int audytorija;

    public Rozklad (string disciplina, int audytorija, int hour, int minute, int second) :base(hour, minute, second)
    {
        this.disciplina = disciplina;
        this.audytorija = audytorija;
    }

    public bool IsPochatok(int hour, int minute)
    {
        if(this.hour==hour && this.minute==minute)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public string Info()
    {
        return disciplina + ", aud." + audytorija +", time "+ InfoTime();
    }
}


