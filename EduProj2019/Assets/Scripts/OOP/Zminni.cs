﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zminni : MonoBehaviour
{
    const float pi = 3.1415f;

    public int number;
    public float number2;

    // Start is called before the first frame update
    void Start()
    {
        // цілі типи даних
        int cile = 1;
        byte cileMale = 5;
        cile = cileMale; // 32 > 8 ok
        cileMale = (byte)cile; // 8 < 32 ne ok *error*
        // 0000 0000 bute
        // 0000 0000 0000 0010 0000 0100 0100 0000 int 
        //string cileMale1 = cile.ToString();


        // дробові типи даних
        float drib1 = 0.5f;
        double drib2 = 5;



        // символ
        char symvol = '+';

        // рядок (багато символів)
        string stroka = "Hello";

        // булевий/логічний
        bool logic = false; // true | false

        // не явно типізовані локальні змінні
        var value = 4;

        cile = number;

        // 0 - 0000 0000
        // 1(2^0=1) - 0000 0001
        // 2(2^1=2) - 0000 0010
        // 3 - 0000 0011
        // 4(2^2=4) - 0000 0100
        // 8(2^3=8) - 0000 1000 
        // 9 - 0000 1001
        // 16 (2^4) - 0001 0000
        // 32 (2^5) - 0010 0000
        // 64 (2^6) - 0100 0000
        // 128(2^7) - 1000 0000

        // 75 - 0100 1011

        // 0001 0101 - 1+4+16=21

        // Операції над змінними
        // + 
        // a+b=c

        cile = cile + cileMale;
        // 6  =  1   +   5 

        //- * 
        // /

        //int d = 3 / 5;  // =0
        //float d = 3 / 5;  // =0
        //float d = 3 / 5f;  // =0.6

        int m = 12457; // вага в кг
        int mt = m / 1000; // 12 тон
        //print(mt);

        // % остача від ділення
        // 7 % 2 = 1 (2 2 2 1)
        // 4 % 2 = 0

        // a=a+5;
        // a+=5;

        // a=a+1;
        // a++;
        // ++a;

        // a=2;
        // b=3;
        // c = ++a + b; // c=6 , a=3, b=3

        // a=2;
        // b=3;
        // c = a++ + b; // a+b=5 -> c=5 , a+1=3 -> a=3 , b=3

        // a=a-1;
        // a--;
        // --a;

        float val = pi * 2;
        // pi = 3f; // помилка

        val = Mathf.Sqrt(9f); // корінь квадратний з 9 = 3
        val = Mathf.Pow(2f, 4f); // 2 в степені 4 = 16
        val = Mathf.Abs(-5); // модуль з -5 = 5, модуль з 8 = 8
        val = Mathf.Cos(5f);
        val = Mathf.Sin(3f);

        int a = Random.Range(1,100); // від 1 до 99
        int b = Random.Range(1, 100);
        int c = Random.Range(1, 100);
        int x = Random.Range(1, 100);
        print("a = " + a + ",  b = " + b + ",  c = " + c + ",  x = " + x);

        float rezul1 = 1 / Mathf.Sqrt(a * Mathf.Pow(x, 2) + b * x + c);
        print("Zavdanja 2a " + rezul1);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
