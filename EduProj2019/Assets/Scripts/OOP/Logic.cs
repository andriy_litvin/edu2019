﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logic : MonoBehaviour
{
    public int n;
    public int c1;
    public int c2;

    void Start()
    {
        bool res = false;
        res = 2 < 5; // true
        print(res);
        res = res && true; // true
        res = !res || false; // false
        print(res);

        // | - або
        //a= 1000 0101
        //b= 0100 0100
        //|= 1100 0101

        // & - i
        //a= 1000 0101
        //b= 0100 0100
        //&= 0000 0100

        // ^ - виключне або
        //a= 1000 0101
        //b= 0100 0100
        //^= 1100 0001

        // ~ - заперечення
        //a= 1000 0101
        //~= 0111 1010

        // << - зсув в ліво
        // a =   1000 0101
        //a<<2 = 0001 0100

        // 2  =  0000 0010
        // 2<<3= 0001 0000 

        // >> - зсув в право
        // a =   1000 0101
        //a>>2 = 1110 0001


        uint a = 2 << 3;
        print(a);// a=16;

        a = ~a;
        print(a);

        /*
        int num = 2;
        if (num > 10)
        {
            print(num + " > 10");
            print("+");
        }
        else 
        {
            print(num + " <= 10");
        }*/

        // перевірити число на парність

        if(n % 2 == 0)
        {
            print("parne");
        }
        else 
        {
            print("ne parne");
        }

        // Якщо число менше 10 то множимо його на 2
        // якщо число більше рівне 10 і менше 20 то додаємо 10
        // якщо число більше рівне 20 то ділимо на 2

        if (n < 10)
            n *= 2;
        else if (n >= 10 && n < 20)
            n += 10;
        else
            n /= 2;

        print("n = "+n);

        // визначити яке з чисел більше, а яке менше (c1, c2)
        // та в найбільшого визначити чи воно парне
        if (c1 > c2)
        {
            print(c1 + " > " + c2);

            if (c1 % 2 == 0)
            {
                print(c1 + " parne");
            }
            else
            {
                print(c1 + " ne parne");
            }
        }

        else if (c1 < c2)
        {
            print(c1 + " < " + c2);
            if (c2 % 2 == 0)
            {
                print(c2 + " parne");
            }
            else
            {
                print(c2 + " ne parne");
            }
        }
        else
            print(c1 + " = " + c2);


        // за порядковим номером дня тижня визначити який це день 

        int deys = Random.Range(1, 10);
        print("deys = " + deys);

        switch (deys)
        {
            case 1:
                print("ponedilok");
                break;
            case 2:
                print("vivtorok");
                break;
            case 3:
                print("sereda");
                break;
            case 4:
                print("chetver");
                break;
            case 5:
                print("pjatnuza");
                break;
            case 6:
                print("subota");
                break;
            case 7:
                print("nedilja");
                break;
            default:
                print("takoho dnja nema");
                break;
        }

        // за порядковим номером визначити це робочий день чи вихідний

        switch (deys)
        {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                print("robochuy");
                break;
            case 6:
            case 7:
                print("vuhidnuy");
                break;
            default:
                print("*takoho dnja nema");
                break;
        }

        string sex = "M";

        switch (sex)
        {
            case "M":
                print("Men");
                break;
            case "W":
                print("Women");
                break;
            default:
                print("error: sex");
                break;
        }


        // якщо число парне, то помножити його на 2, а якщо не парне помножити на себе

        c1 = c1 % 2 == 0 ? c1 * 2 : c1 * c1;

        if (c1 % 2 == 0)
            c1 *= 2; // c1=c1*2;
        else
            c1 *= c1;



        // завдання 11

        int num = 25;

        int oper = 1; // 0001 = 1
        int rezul = 0;

        string rezBit = "";

        // 0-bit
        rezul = num & oper;
        rezBit = (rezul == oper ? "1" : "0") + rezBit;

        // 1-bit
        oper = oper << 1; // 0010 = 2
        rezul = num & oper;
        if (rezul == oper)
            rezBit = "1" + rezBit;
        else
            rezBit = "0" + rezBit;

        // 2-bit
        oper = oper << 1; // 0100 = 4
        rezul = num & oper;
        rezBit = (rezul == oper ? "1" : "0") + rezBit;

        // 3-bit
        oper = oper << 1; // 1000 = 8
        rezul = num & oper;
        rezBit = (rezul == oper ? "1" : "0") + rezBit;

        // 4-bit
        oper = oper << 1; 
        rezul = num & oper;
        rezBit = (rezul == oper ? "1" : "0") + rezBit;

        // 5-bit
        oper = oper << 1; 
        rezul = num & oper;
        rezBit = (rezul == oper ? "1" : "0") + rezBit;

        // 6-bit
        oper = oper << 1; 
        rezul = num & oper;
        rezBit = (rezul == oper ? "1" : "0") + rezBit;

        // 7-bit
        oper = oper << 1; 
        rezul = num & oper;
        rezBit = (rezul == oper ? "1" : "0") + rezBit;

        print("desatkove " + num + " = dviykove " + rezBit);


        // завдання 12
        int n12 = 27;
        int i12 = 2;

        int rezul12 = n12 | (1 << i12);

        print("zav 12 = "+rezul12);

    }


}
