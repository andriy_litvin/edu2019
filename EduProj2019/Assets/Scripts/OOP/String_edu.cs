﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class String_edu : MonoBehaviour
{
  
    void Start()
    {
        /*
        string str = "Hello";
        string str2 = str + " Unity"; // Hello Unity
        str += str2; // HelloHello Unity
        string gol = "aouiey";       

        print(str[1]); // e

        // В рядку str знайти лількість символів "о"
        int countO = 0;
        for(int i=0; i < str.Length; i++)
        {
            if(str[i]=='o')
            {
                countO++;
            }
        }
        print("Count \"o\" = "+countO);  //   \n    \t 

        string s1 = null;
        string s2 = "";
        string s3 = "Unity";
        string s4 = "\t";

        print(string.IsNullOrEmpty(s1));
        print(string.IsNullOrEmpty(s2));
        print(string.IsNullOrEmpty(s3));
        print(string.IsNullOrEmpty(s4));

        print(string.IsNullOrWhiteSpace(s1));
        print(string.IsNullOrWhiteSpace(s2));
        print(string.IsNullOrWhiteSpace(s3));
        print(string.IsNullOrWhiteSpace(s4));


        s3 = s3.ToUpper(); // UNITY
        print(s3);

        s3 = s3.ToLower(); // unity
        print(s3);

        str = "Hello Unity";
        if( str.Contains("Hello"))
        {
            print("znaydeno");
        }
        else
        {
            print("ne znaydeno");
        }

        print( str.IndexOf("ni")); // 7 
        print(str.IndexOf("aaa")); // -1

        str = str.Insert(5, ","); // Hello, Unity
        print(str);

        print(str.Remove(5)); // Hello
        print(str.Remove(5, 2)); // HelloUnity

        print(str.Substring(7)); // Unity
        print(str.Substring(5,2)); // ", " 

        str = "Hello Unity, Hello World";
        print(str.Replace("Hello", "Hi"));

        char[] masChar = str.ToCharArray();
        string pr = "";
        for(int i=0; i<masChar.Length; i++)
        {
            pr += "'" + masChar[i] + "'\t"; 
        }
        print(pr);

        str = "Sasha,Pasha,Masha,Ivan";
        string[] masStr = str.Split(',');
        pr = "";
        for (int i = 0; i < masStr.Length; i++)
        {
            pr += "'" + masStr[i] + "'\t";
        }
        print(pr);
        */

        string str = "яблоко"; // блок  око
        string str1 = str.Substring(1, 4);
        string str2 = str.Substring(3);

        print(str1 + "\t" + str2);


        str = "вертикаль"; // тир   ветка
        str1 = str.Substring(3,2)+str[2];
        str2 = str.Substring(0, 2) + str[3] + str.Substring(5,2);
        print(str1 + "\t" + str2);

        str = "курсор"; // танцор
        str = str.Replace("курс", "танц");
        print(str);


        // знайти слово, що має найменшу довжину 
        str = "Hello Unity, Hi World";

        string[] masStr = str.Split(' ', ',');

        int minLen = 1000;
        string minStr = "";

        for(int i=0; i<masStr.Length; i++)
        {
            if(minLen>masStr[i].Length && masStr[i].Length>0)
            {
                minStr = masStr[i];
                minLen = masStr[i].Length;
            }
        }
        print(minStr + "\t" + minLen);


        // видалення подвоєння
        str = "Hello   Woooorldd";

        for(int i=0; i<str.Length-1; i++)
        {
            if (str[i] == str[i+1]) 
            {
                str = str.Remove(i, 1);
                i--;
            }
        }
        print(str);

        // 12
        str = "Hello world";
        str2 = "";

        for(int i=str.Length-1; i>=0; i--)
        {
            str2 += str[i];
        }
        print(str2);


    }


}
