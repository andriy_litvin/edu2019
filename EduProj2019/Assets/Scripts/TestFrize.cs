﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFrize : MonoBehaviour
{
    float timer;
    bool isFrize;
    SpriteRenderer spr;

    // Start is called before the first frame update
    void Start()
    {
        timer = 1;
        isFrize = false;
        spr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isFrize)
        {
            timer -= Time.deltaTime;

            if (timer < 0)
            {
                if (spr.color == Color.red)
                {
                    spr.color = Color.green;
                }
                else
                {
                    spr.color = Color.red;
                }
                timer = Random.Range(1f, 3f);
            }
        }

    }

    private void OnMouseDown()
    {
        if(spr.color == Color.red)
        {
            isFrize = true;
        }
    }
}
