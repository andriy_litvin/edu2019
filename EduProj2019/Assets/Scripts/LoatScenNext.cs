﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoatScenNext : MonoBehaviour
{
    public int nextLevel;
    bool isActiv = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Next level! Press F");
        isActiv = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isActiv = false;
    }

    private void Update()
    {
        if(isActiv && Input.GetKeyDown(KeyCode.F))
        {           
            UnityEngine.SceneManagement.SceneManager.LoadScene(nextLevel);
        }

    }

}
