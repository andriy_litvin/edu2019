﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestObj : MonoBehaviour
{
    [ContextMenuItem("Reset step", "ResetStep")]
    public int step;

    public Vector3 vec3;
    public SpriteRenderer kolobokSR;
    //public Transform[] transArray;

    void ResetStep()
    {
        step = 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        kolobokSR.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }

    private void OnMouseEnter()
    {
        transform.localScale += new Vector3(step, step, step);
    }

    private void OnMouseExit()
    {
        transform.localScale -= new Vector3(step, step, step);
    }
}
