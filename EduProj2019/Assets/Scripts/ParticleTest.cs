﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParticleTest : MonoBehaviour
{
    ParticleSystem ps;
    List<ParticleCollisionEvent> collEvent = new List<ParticleCollisionEvent>();
    List<ParticleSystem.Particle> enterPar = new List<ParticleSystem.Particle>();

    private void OnEnable()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void OnParticleCollision(GameObject other)
    {
        if(other.tag == "stina")
        {
            int countColl = ps.GetCollisionEvents(other, collEvent);
            Rigidbody rb = other.GetComponent<Rigidbody>();
            for(int i=0; i<countColl; i++)
            {
                rb.AddForce(collEvent[i].velocity*100);
            }
        }
    }

    private void OnParticleTrigger()
    {
        int countEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enterPar);
        for(int i=0; i<countEnter; i++)
        {
            ParticleSystem.Particle par = enterPar[i];
            par.startColor = Color.red;
            enterPar[i] = par;
        }
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enterPar);
    }

    void OnParticleSystemStopped()
    {
        print("particle stop "+gameObject.name);
        Destroy(gameObject);
    }


}
