﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameParams : MonoBehaviour
{
    public string loginUser;
    public int level;
    public int score;

    /* Лінивий Сінглетон
    public static GameParams singletone { get; private set; }

    private void Awake()
    {
        singletone = this;
    }
    */

    /* Стандартний Сінглетон 
    private static GameParams _singletone;
    public static GameParams singletone
    {
        get
        {
            if (!_singletone)
            {
                _singletone = FindObjectOfType<GameParams>();
            }
            return _singletone;
        }
    }
    */

    // Стійкий Сінглетон
    private static GameParams _singletone;
    public static GameParams singletone
    {
        get
        {
            if (!_singletone)
            {
                _singletone = FindObjectOfType<GameParams>();
                DontDestroyOnLoad(_singletone.gameObject);
            }
            return _singletone;
        }
    }

    private void Awake()
    {
        if (!_singletone)
        {
            _singletone = this;
            DontDestroyOnLoad(_singletone.gameObject);
        }
        else if(this != _singletone)
        {
            Destroy(this.gameObject);
        }
    }

}
