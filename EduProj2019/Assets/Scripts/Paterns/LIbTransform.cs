﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LIbTransform 
{
    public static void SetPositionX(this Transform trans, float x)
    {
        trans.position = new Vector3(x, trans.position.y, trans.position.z);
    }

}
