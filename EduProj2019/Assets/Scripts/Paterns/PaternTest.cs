﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaternTest : MonoBehaviour
{
    [System.Flags]
    public enum Stan
    {
        Normal= 1<<0,   // 1 = 0001
        Poison= 1<<1,   // 2 = 0010
        Tier =  1<<2,   // 4 = 0100
        Energy= 1<<3    // 8 = 1000
    }

    public Stan stan;

    void Start()
    {
        print(stan);
        // вмикаємо лише стан Normal
        stan = Stan.Normal;
        print(stan);

        if (stan == Stan.Normal)
        {
            print("Активний лише один стан Normal 1 ");
        }
        // вмикаємо додатковий стан Tier
        stan |= Stan.Tier;
        // 0001 | 0100 = 0101
        print(stan);
        // вимикаємо стан Normal
        stan &= ~Stan.Normal;
        // 0101 
        // 1110 <- ~0001
        // 0100
        print(stan);
        // вмикаємо стан Energy
        stan |= Stan.Energy;
        print(stan);

        if (stan==Stan.Normal)
        {
            print("Активний лише один стан Normal 2 ");
        }
        // 1010 & 1000 = 1000
        if((stan & Stan.Energy)==Stan.Energy)
        {
            print("Активний стан Energy");
        }

        Transform[,][] mas = new Transform[4, 5][];
        var par = mas;
    }

    // обнулюючий тип
    int? posX = null;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F3))
        {
            posX = Random.Range(-5, 5);
        }

        if(posX.HasValue)
        {
            print("move to " + posX);
            //transform.position = new Vector3((float)posX, transform.position.y, transform.position.z);
            transform.SetPositionX((float)posX);
            posX = null;
        }
    }
}
