﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSing : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F1))
        {
            GameParams.singletone.loginUser = "User" + Random.Range(1, 10);
            GameParams.singletone.level = Random.Range(0, 10);
            GameParams.singletone.score = Random.Range(0, 100);
        }
        else if(Input.GetKeyDown(KeyCode.F2))
        {
            print(GameParams.singletone.loginUser + " -  Level: " + GameParams.singletone.level
                + "; Score: " + GameParams.singletone.score);
        }

    }
}
