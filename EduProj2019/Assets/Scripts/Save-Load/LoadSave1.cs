﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadSave1 : MonoBehaviour
{
    public Transform player;
    public Rigidbody bulet;
    public Transform cube;
    public GameObject[] objs;

    void Start()
    {
        bulet = Resources.Load<Rigidbody>("Bullet");
        cube = Resources.Load<Transform>("3d obj/Cube");
        objs = Resources.LoadAll<GameObject>("");

        LoadFile();

        PlayerPrefs.SetInt("level", 1);
        print(PlayerPrefs.GetInt("level"));
        PlayerPrefs.SetString("gameSave", "2;5;8;22;43");
    }

    void Update()
    {
        // Збереження
        if(Input.GetKeyDown(KeyCode.F1))
        {
            string strPos = "";
            for(int i=0; i<3; i++)
            {
                strPos += player.position[i] + ";";
            }

            for (int i = 0; i < 3; i++)
            {
                strPos += player.eulerAngles[i] + ";";
            }
            strPos = strPos.Remove(strPos.Length - 1);
            print(strPos);
            PlayerPrefs.SetString("pos", strPos);
        }
        // Завантаження
        else if(Input.GetKeyDown(KeyCode.F2))
        {
            string[] arrayPos = PlayerPrefs.GetString("pos").Split(';');
            Vector3 pos = Vector3.zero;
            Vector3 rot = Vector3.zero;

            for(int i=0; i<3; i++)
            {
                pos[i] = float.Parse(arrayPos[i]);
            }
            player.position = pos;

            for(int i=3; i<6; i++)
            {
                rot[i-3] = float.Parse(arrayPos[i]);
            }
            player.eulerAngles = rot;
        }


    }

    void LoadFile()
    {
        string fileStr, path;

#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.streamingAssetsPath, "test.txt");
        WWW read = new WWW(path);
        while (!read.isDone) { }
        fileStr = read.text;
#endif

#if UNITY_EDITOR
        fileStr = File.ReadAllText(Application.streamingAssetsPath + "/test.txt");
#endif

        print(fileStr);

    }

}
