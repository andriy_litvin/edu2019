﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // створення променя, що пускається з камери
            RaycastHit hit; // оголошення змінної, що зберігатиме інформацію про об'єкт в який потрапив промінь

            if(Physics.Raycast(ray, out hit))// запуск променя
            {
                print(hit.transform.name);  // виводимо назву об'єкта в який потрапив промінь 
                Debug.DrawLine(Camera.main.transform.position, hit.point, Color.cyan, 1); // візуалізація променя

                // створення копії об'єкта в який потрапив промінь з врахуванням зміщення та величини
                Instantiate(hit.transform.gameObject, 
                    hit.point+Vector3.Scale(hit.normal, hit.transform.localScale/2f), 
                    Quaternion.identity);

            }
        }
        else if(Input.GetMouseButton(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                hit.transform.localScale *= Input.GetAxis("Mouse Y") * 0.03f + 1;
                // 1 * 0.03 = 0.03
                // 1 * 1.03 = 1.03
                // 1 * 0.97 = 0.97
            }

        }
    }
}
