﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjHidde : MonoBehaviour
{
    HiddenObjGame game;

    void Start()
    {
        game = GetComponentInParent<HiddenObjGame>();        
    }

    void OnMouseDown()
    {
        if(game.objToFind==name)
        {
            print("***" + name + "***");
            game.NextObjToFind();
            Destroy(gameObject, 0.5f);
        }
        else
        {
            print("ne to");
        }
    }
}
