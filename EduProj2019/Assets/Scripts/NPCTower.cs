﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCTower : MonoBehaviour
{
    public Rigidbody bullet;
    public float radiusAtack = 5f;
    Transform shotPoint;
    bool isShot = true;

    void Start()
    {
        shotPoint = transform.Find("shot point");        
    }

    void Update()
    {
        if (isShot)
        {
            if (Vector3.Distance(transform.position, PlayerController3d.player.transform.position) < radiusAtack)
            {
                shotPoint.LookAt(PlayerController3d.player.transform.position);
                ShotTower(PlayerController3d.player.transform);
            }
            else
            {
                shotPoint.localEulerAngles = new Vector3(0, Random.Range(0f, 360f), 0);
                ShotTower(null);
            }
        }

    }

    void ShotTower(Transform mishen)
    {
        Rigidbody temp = Instantiate(bullet, shotPoint.position, Quaternion.identity);
        temp.AddForce(shotPoint.TransformDirection(0, 0, 1000));
        temp.GetComponent<NPCBullet>().target = mishen; 
        isShot = false;
        StartCoroutine(TimerShot());
    }

    IEnumerator TimerShot()
    {
        yield return new WaitForSeconds(2);
        isShot = true;
    }
}
