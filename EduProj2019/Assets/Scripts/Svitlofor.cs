﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Svitlofor : MonoBehaviour
{
    public SpriteRenderer[] lamps;
    int lampActiv;
    Color defColor;
    float timer;
    bool nextGreen; // true - green,    false - red

    void Start()
    {
        defColor = lamps[0].color;
        lampActiv = 1;
        lamps[0].color = Color.red;
        timer = 0;
        nextGreen = true;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (lampActiv==1)
        { 
            if(timer>3)
            {
                timer = 0;
                lamps[1].color = Color.yellow;
                lamps[0].color = defColor;
                lampActiv = 2;
                nextGreen = true;
            }
        }
        else if(lampActiv==2)
        {
            if(timer>1)
            {
                timer = 0;
                lamps[1].color = defColor;

                if(nextGreen)
                {
                    lamps[2].color = Color.green;
                    lampActiv = 3;
                }
                else
                {
                    lamps[0].color = Color.red;
                    lampActiv = 1;
                }

            }
        }
        else if(lampActiv==3)
        {
            if (timer > 3) 
            {
                timer = 0;
                lamps[1].color = Color.yellow;
                lamps[2].color = defColor;
                lampActiv = 2;
                nextGreen = false;
            }
        }


    }
}
