﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPS : MonoBehaviour
{
    public float speed=1;
    public float radiusSensor=0.2f;
    public Transform[] sensors;
    public LayerMask layerStina;
    int hp = 2;
    Rigidbody2D rb;
    Vector2 napramok;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        napramok = Vector2.up;
    }

    // Update is called once per frame
    void Update()
    {
        if(napramok==Vector2.up && Physics2D.OverlapCircle(sensors[0].position, radiusSensor, layerStina))
        {
            Rot();
            //print("0");
        }
        else if (napramok == Vector2.right && Physics2D.OverlapCircle(sensors[1].position, radiusSensor, layerStina))
        {
            Rot();
            //print("1");
        }
        else if (napramok == Vector2.down && Physics2D.OverlapCircle(sensors[2].position, radiusSensor, layerStina))
        {
            Rot();
            //print("2");
        }
        else if (napramok == Vector2.left && Physics2D.OverlapCircle(sensors[3].position, radiusSensor, layerStina))
        {
            Rot();
            //print("3");
        }
        rb.velocity = napramok * speed * Time.deltaTime;
    }

    void Rot()
    {
        while (true)
        {
            int rand = Random.Range(0, 4);

            if (rand == 0 && !Physics2D.OverlapCircle(sensors[0].position, radiusSensor, layerStina))
            {
                napramok = Vector2.up;
                //print("*0");
                return;
            }
            else if (rand == 1 && !Physics2D.OverlapCircle(sensors[1].position, radiusSensor, layerStina))
            {
                napramok = Vector2.right;
                //print("*1");
                return;
            }
            else if (rand == 2 && !Physics2D.OverlapCircle(sensors[2].position, radiusSensor, layerStina))
            {
                napramok = Vector2.down;
                //print("*2");
                return;
            }
            else if (rand == 3 && !Physics2D.OverlapCircle(sensors[3].position, radiusSensor, layerStina))
            {
                napramok = Vector2.left;
                //print("*3");
                return;
            }
        }
    }

    public void MinusHP(Mario mario)
    {
        hp--;
        if(hp==1)
        {
            GetComponent<SpriteRenderer>().color = Color.blue;
        }
        else if(hp<=0)
        {
            Destroy(gameObject);
            mario.KillNps();
        }
    }
}
