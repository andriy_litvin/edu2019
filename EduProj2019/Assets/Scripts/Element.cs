﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element : MonoBehaviour
{
    public int idI, idJ;
    public SpriteRenderer sprRen;
    StartGame game; // посилання для доступу до головного скрипта

    // Start is called before the first frame update
    void Start()
    {
        sprRen = GetComponent<SpriteRenderer>();
        game = GetComponentInParent<StartGame>();
    }

    private void OnMouseEnter()
    {
        sprRen.color = Color.red;

        for(int i=idI-1; i<=idI+1; i++)
            for(int j=idJ-1; j<=idJ+1; j++)
            {
                if (game.elems[i,j] && game.elems[i, j]!=this)
                {
                    game.elems[i,j].sprRen.color = Color.green;
                }
            }
    }

    private void OnMouseExit()
    {
        for (int i = idI - 1; i <= idI + 1; i++)
            for (int j = idJ - 1; j <= idJ + 1; j++)
            {
                if (game.elems[i, j])
                {
                    game.elems[i, j].sprRen.color = Color.white;
                }
            }
    }
}
