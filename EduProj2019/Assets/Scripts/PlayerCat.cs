﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCat : MonoBehaviour
{
    public float speed = 1;
    public Transform sensGround;
    public LayerMask layerGround;
    //public Rigidbody2D kirka;
    public float speedKirka=50;
    SpriteRenderer sr;
    Rigidbody2D rb;
    Animator anim;
    bool isGround;
    bool isRight;
    float move;
    bool isShoot;

    Paralax par;
    Transform cam;
    Vector3 deltaCamPos;
    Vector3 newPosCam;
    float posX, posY;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        isRight = true;
        isGround = false;
        isShoot = true;

        cam = Camera.main.transform;
        deltaCamPos = cam.position - transform.position;
        par = cam.GetComponentInChildren<Paralax>();
        posX = transform.position.x;
        posY = transform.position.y;
    }

    void FixedUpdate()
    {
        // перевіряємо чи ми на землі через "сенсор"
        isGround = Physics2D.OverlapCircle(sensGround.position, 0.1f, layerGround);
        // передаємо значення isGround в Animator
        anim.SetBool("isGround", isGround);
        // передаємо значення швидкості руху по осі "у" в Animator
        anim.SetFloat("ySpeed", rb.velocity.y);

        // записуюмо в змінну значення віртуальної осі, що відповідає за горизонтальне переміщення
        move = Input.GetAxis("Horizontal");
        // передаємо значення швидкості руху по модулю по осі "х" в Animator
        anim.SetFloat("xSpeed", Mathf.Abs(move));

        rb.velocity = new Vector2(move * speed, rb.velocity.y);

        if(move>0 && !isRight)
        {
            sr.flipX = false;
            isRight = true;
        }
        else if(move < 0 && isRight)
        {
            sr.flipX = true;
            isRight = false;
        }
    }


    void Update()
    {
        newPosCam = transform.position + deltaCamPos;
        cam.position = newPosCam;
        par.run = (transform.position.x-posX) * 20f;
        posX = transform.position.x;

        par.jump = transform.position.y - posY;
        posY = transform.position.y;

        if (isGround && Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("isGround", false);
            rb.AddForce(new Vector2(0, 500));
        }
        else if(Input.GetKeyDown(KeyCode.Return) /*&& isShoot*/)
        {
            //Rigidbody2D temp = Instantiate(kirka, transform.position, Quaternion.identity);
            Rigidbody2D temp = PoolSys.sys.TakeFromPool(0,transform.position).GetComponent<Rigidbody2D>();
            temp.AddForce( (isRight ? Vector2.right : Vector2.left) * speedKirka);
            isShoot = false;
            //StartCoroutine(TimeShoot());
        }
    }

    public void Death()
    {
        Camera.main.transform.parent = null;
        //Destroy(gameObject);
        sr.enabled = false;
        GetComponent<CapsuleCollider2D>().enabled = false;
        rb.simulated = false;
        anim.enabled = false;
        StartCoroutine(RestartLevel());
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.collider.tag=="nps")
        {
            Death();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="prirva")
        {
            Death();
        }
    }

    IEnumerator TimeShoot()
    {
        yield return new WaitForSeconds(1);
        isShoot = true;
    }

    IEnumerator RestartLevel()
    {
        yield return new WaitForSeconds(3);
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
}
