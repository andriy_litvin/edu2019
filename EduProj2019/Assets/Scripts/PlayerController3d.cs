﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController3d : MonoBehaviour
{
    public float speed = 3f;
    public Rigidbody bullet;
    public Transform gan;
    public static PlayerController3d player;
    Rigidbody rb;
    Transform cam;
    Vector3 rotCam;
    int hp = 100;

    public AudioClip[] clips;
    AudioSource source;
    public LayerMask mask;
    public AudioSource audioFon;

    private void Awake()
    {
        player = this;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main.transform;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        source = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        rb.velocity = transform.TransformDirection(Input.GetAxis("Horizontal") * speed, rb.velocity.y, Input.GetAxis("Vertical") * speed);
        transform.Rotate(0, Input.GetAxis("Mouse X"), 0, Space.World);
        rotCam = cam.localEulerAngles;
        cam.Rotate(-Input.GetAxis("Mouse Y"), 0, 0, Space.Self);

        if((cam.localEulerAngles.x>310 && cam.localEulerAngles.x<360)|| (cam.localEulerAngles.x > 0 && cam.localEulerAngles.x < 30))
        {
        }
        else
        {
            cam.localEulerAngles = rotCam;
        }

        if(rb.velocity.magnitude > 1 && !source.isPlaying)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, Vector3.down, out hit, 1.2f, mask))
            {
                if(hit.collider.sharedMaterial.name=="grass")
                {
                    source.clip = clips[0];
                }
                else 
                {
                    source.clip = clips[1];
                }
                source.Play();
            }

        }

    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && Physics.Raycast(transform.position,Vector3.down,1.2f))
        {
            rb.AddForce(0, 300, 0);
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }

        if (Input.GetMouseButtonDown(0))
        {
            source.PlayOneShot(clips[2]);
            Rigidbody temp = Instantiate(bullet, gan.position, Quaternion.identity);
            temp.AddForce(gan.TransformDirection(0, 0, 800));
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = Time.timeScale == 1 ? 0 : 1;

            if(audioFon.isPlaying)
            {
                audioFon.Pause();
            }
            else
            {
                audioFon.Play();
            }
        }

    }

    public void Damage(int damage)
    {
        hp -= damage;
        print("HP: " + hp);
        if(hp<=0)
        {
            print("GAME OVER");
            Time.timeScale = 0; // аналог паузи - значення 0 зупиняє гру, а 0,5 - робить її уповільненою
        }
    }
}
