﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCUB : MonoBehaviour
{
    public int sizeCub=4; // 4x4x4 
    public MeshRenderer cub;

    // Start is called before the first frame update
    void Start()
    {
        for(int i=0; i<sizeCub; i++)
        {
            for(int j=0; j<sizeCub; j++)
            {
                for(int k=0; k<sizeCub; k++)
                {
                    MeshRenderer temp = Instantiate(cub, new Vector3(i, j, k), Quaternion.identity, transform);
                    temp.material.color = 
                        new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
