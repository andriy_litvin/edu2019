﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestITween : MonoBehaviour
{
    //public Texture2D texture;
    public Transform[] path;

    void Start()
    {
        // Перміщення в вказану позицію
        //iTween.MoveTo(gameObject, new Vector3(0, 1, 0), 2);
        //iTween.MoveTo(gameObject, iTween.Hash("y", 2, "time", 2));

        // 
        //iTween.MoveTo(gameObject, iTween.Hash("y", 2, "time", 2,"delay",1,
        //"looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.easeInOutBounce));

        // Зміщення на вказану величину
        //iTween.MoveAdd(gameObject, iTween.Hash("x", 2, "time",3));
        //iTween.MoveAdd(gameObject, iTween.Hash("y", 2, "time", 6,"delay",3));

        // Переміщення з вказаної позиції в поточну
        //iTween.MoveFrom(gameObject, iTween.Hash("x", 2, "speed", 0.5));

        // Зміна розміру
        //iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(2, 2, 2), "time", 3));

        // Тряска об'єкта
        //iTween.ShakePosition(gameObject, iTween.Hash("x", 2, "time", 3, "delay", 1));

        // Зміна прозорості (настройка матеріалу - rendering mode: fade)
        //iTween.FadeTo(gameObject, 0, 3);

        Camera.main.transform.position = path[0].position;
        // Зміна кольору
        iTween.ColorTo(gameObject, iTween.Hash("color", Color.red, "looptype", iTween.LoopType.pingPong,
            "time", 2, "delay", 1));

        // Переміщення по шляху
        iTween.MoveTo(Camera.main.gameObject, iTween.Hash("path",path,"time",30, "easetype", iTween.EaseType.linear,
            "orienttopath",true,"looptype", iTween.LoopType.loop));

        // Переміщення в вказану точку та по завершенні анімації виклик момоду Finish з параметром 5 "Finish(5)"
        iTween.MoveTo(gameObject, iTween.Hash("y", 2, "time", 2, "oncomplete", "Finish", "oncompleteparams",5));
               
    }

    void Update()
    {
        
    }

    void OnDrawGizmos()
    {
        // Візуалізація шляху для відладки
        iTween.DrawPath(path, Color.red);
    }

    void Finish(int val)
    {
        print("Animation complete "+val);
    }
}
