﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mario : MonoBehaviour
{
    public int hp=3;
    public float speed = 1;
    public int countBombs = 0;
    public Rigidbody2D boolet;
    public float speedBoolet = 10;
    public int score = 0;
    public Transform groupNps;
    Rigidbody2D rb;
    SpriteRenderer sr;
    Vector2 napramok;
    bool isRight=true;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        napramok = Vector2.right;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && countBombs>0)
        {
            Rigidbody2D temp = Instantiate(boolet, transform.position, Quaternion.identity);
            temp.AddForce(napramok * speedBoolet);
            countBombs--;
            temp.GetComponent<Boolet>().mario = this;
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            napramok = Vector2.left;
            Flip();
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            napramok = Vector2.right;
            Flip();
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb.velocity = new Vector2(rb.velocity.x, speed);
            napramok = Vector2.up;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.velocity = new Vector2(rb.velocity.x, -speed);
            napramok = Vector2.down;
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag=="bomba")
        {
            Destroy(coll.gameObject);
            countBombs+=3;
        }
    }

    void Flip()
    {
        if(isRight && napramok==Vector2.left)
        {
            isRight = false;
            sr.flipX = true;
        }
        else if(!isRight && napramok==Vector2.right)
        {
            isRight = true;
            sr.flipX = false;
        }

    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.collider.tag=="nps")
        {
            MinusHP();

        }
    }

    public void KillNps()
    {
        score++;
        print("Score: " + score);
        //print(groupNps.childCount);
        if(groupNps.childCount==1)
        {
            print("WIN!");
        }
    }

    void MinusHP()
    {
        hp--;

        switch(hp)
        {
            case 2:
                sr.color = Color.red;
                break;
            case 1:
                sr.color = Color.green;
                break;
            case 0:
                Destroy(gameObject);
                print("GAME OVER");
                break;
        }
    }
}
