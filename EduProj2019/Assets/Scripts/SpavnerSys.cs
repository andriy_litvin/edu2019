﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpavnerSys : MonoBehaviour
{
    public static SpavnerSys spSys;
    public Transform npc;  // посилання на префаб суперника
    public int maxCountNps;  // максимальна кількість 
    Transform[] spavners; // точка спавну
    int countNps;   // кількість живих НПС 
    bool[] isPlatformActiv;

    void Start()
    {
        spSys = this;
        Transform[] temp = GetComponentsInChildren<Transform>();
        spavners = new Transform[temp.Length - 1];
        int k = 0;
        for(int i=0; i<temp.Length; i++)
        {
            if (temp[i] == transform)
                continue;
            else
            {
                spavners[k] = temp[i];
                k++;
            }
        }

        isPlatformActiv = new bool[spavners.Length];
        if(maxCountNps>spavners.Length)
        {
            maxCountNps = spavners.Length;
        }

        while(countNps < maxCountNps)
        {
            AddNpc();
        }
    }

    void AddNpc()
    {
        int rand = Random.Range(0, spavners.Length);
        if(!isPlatformActiv[rand])
        {
            isPlatformActiv[rand] = true;
            countNps++;
            Transform temp = Instantiate(npc, spavners[rand].position,Quaternion.identity);
            temp.GetComponent<CatNPC>().id = rand;
        }
    }

    public void MinusNpc(int id) 
    {
        isPlatformActiv[id] = false;
        countNps--;
        StartCoroutine(TimerNextNpc());
    }

    IEnumerator TimerNextNpc()
    {
        yield return new WaitForSeconds(5);
        while (countNps < maxCountNps)
        {
            AddNpc();
        }
    }
}
