﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatNPC : MonoBehaviour
{
    public float speed = 1;
    public LayerMask layerGround;
    public LayerMask layerPlayer;
    //public Rigidbody2D sokyra;
    public float speedSokyra = 50;
    public int id;
    Transform sensGround;
    Transform sensorRight;
    Transform sensorLeft;
    SpriteRenderer sr;
    Rigidbody2D rb;
    Animator anim;
    bool isGround;
    bool isRight;
    float move;
    bool isShoot;

    // Start is called before the first frame update
    void Start()
    {
        sensorRight = transform.Find("sensorRight");
        sensorLeft = transform.Find("sensorLeft");
        sensGround = transform.Find("sensorGround");

        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        isRight = true;
        isGround = false;
        isShoot = true;
        move = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (isShoot && Physics2D.Raycast(transform.position, isRight ? Vector2.right : Vector2.left, 12, layerPlayer))
        {
            isShoot = false;
            StartCoroutine(TimeShot());
            //Rigidbody2D temp = Instantiate(sokyra, transform.position, Quaternion.identity);
            Rigidbody2D temp = PoolSys.sys.TakeFromPool(1, transform.position).GetComponent<Rigidbody2D>();
            temp.AddForce((isRight ? Vector2.right : Vector2.left) * speedSokyra);
        }
    }

    void FixedUpdate()
    {
        // перевіряємо чи ми на землі через "сенсор"
        isGround = Physics2D.OverlapCircle(sensGround.position, 0.1f, layerGround);
        // передаємо значення isGround в Animator
        anim.SetBool("isGround", isGround);
        // передаємо значення швидкості руху по осі "у" в Animator
        anim.SetFloat("ySpeed", rb.velocity.y);

        if(isGround)
        {
            if (isRight && !Physics2D.OverlapCircle(sensorRight.position, 0.1f, layerGround))
            {
                move = -1;
            }
            else if (!isRight && !Physics2D.OverlapCircle(sensorLeft.position, 0.1f, layerGround))
            {
                move = 1;
            }
        }


        // передаємо значення швидкості руху по модулю по осі "х" в Animator
        anim.SetFloat("xSpeed", Mathf.Abs(move));

        rb.velocity = new Vector2(move * speed, rb.velocity.y);

        if (move > 0 && !isRight)
        {
            sr.flipX = false;
            isRight = true;
        }
        else if (move < 0 && isRight)
        {
            sr.flipX = true;
            isRight = false;
        }

    }

    IEnumerator TimeShot()
    {
        yield return new WaitForSeconds(1);
        isShoot = true;
    }


}
