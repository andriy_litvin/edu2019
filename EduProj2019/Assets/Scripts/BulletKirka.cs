﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletKirka : MonoBehaviour
{
    Transform kirka;
    PoolObj poolObj;

    // Start is called before the first frame update
    void Start()
    {
        poolObj = GetComponent<PoolObj>();
        kirka = GetComponentInChildren<Transform>();
        StartCoroutine(RotateKirka());
    }

    IEnumerator RotateKirka()
    {
        while(true)
        {
            yield return null;
            kirka.Rotate(0, 0, 100 * Time.deltaTime);
        }
    }

    private void OnBecameInvisible()
    {
        //Destroy(gameObject);
        poolObj.DestroyObj();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag=="nps")
        {
            SpavnerSys.spSys.MinusNpc(collision.collider.GetComponent<CatNPC>().id);
            Destroy(collision.collider.gameObject);
            //Destroy(gameObject);
            poolObj.DestroyObj();
        }
        else 
        {
            //Destroy(gameObject, 0.5f);
            poolObj.DestroyObj();
        }
    }
}
