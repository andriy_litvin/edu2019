﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    public Transform elem;
    public Element[,] elems; 

    // Start is called before the first frame update
    void Start()
    {
        elems = new Element[8,8];

        for (int i=1; i<7; i++)
        {
            for(int j=1; j<7; j++)
            {
                /*Transform temp = Instantiate(elem, new Vector3(i*0.16f-0.52f, 0, 0), Quaternion.identity, transform);
            elems[i] = temp.GetComponent<Element>();*/
                elems[i,j] = Instantiate(elem, new Vector3(j * 0.16f - 0.52f, 0.52f -i * 0.16f, 0), Quaternion.identity, transform).GetComponent<Element>();
                elems[i,j].idI = i;
                elems[i, j].idJ = j;

            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
