﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitGame : MonoBehaviour
{
    // маркери для визначення області в якій буде виконуватися створення об'єктів
    public Transform marker1; 
    public Transform marker2;
    // об'єкт для створення
    public Transform gameObj;
    public int score;
    float timer;
    float timerGame;

    void Start()
    {
        timer = 0;
        score = 0;
        timerGame = 30;

        // створення 5 випадкових елем. в визначеній області
        for(int i=0; i<5; i++)
        {
            Transform temp = Instantiate(gameObj, new Vector3(Random.Range(marker1.position.x, marker2.position.x),
                    Random.Range(marker1.position.y, marker2.position.y), 0), 
                Quaternion.identity, transform);

            temp.GetComponent<GameObj>().initG = this;

        }
    }

    void Update()
    {
        // оновлення таймера
        timer += Time.deltaTime;
        timerGame -= Time.deltaTime;

        // створення нового спрайта
        if(timer>1) // пройшла 1 секунда часу
        {
            Transform temp = Instantiate(gameObj, new Vector3(Random.Range(marker1.position.x, marker2.position.x),
                    Random.Range(marker1.position.y, marker2.position.y), 0),
                Quaternion.identity, transform);
            temp.GetComponent<GameObj>().initG = this;
            timer = 0;
        }

        if(timerGame<0)
        {
            print("Score: " + score);
            Destroy(gameObject);
        }

        /*// перевірка стану рахунка
        if(score>=10)
        {
            print("WIN, score: " + score);
            Destroy(gameObject); // знищення поточного об'єкта
        }
        else if(score<0)
        {
            print("GAME OVER, score: " + score);
            Destroy(gameObject);
        }*/

    }
}
