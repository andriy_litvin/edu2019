﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC3 : MonoBehaviour
{
    public float speed = 3f;
    public Transform[] points;
    public Rigidbody bullet;
    Transform shotPoint;
    Rigidbody rb;
    int hp = 30;
    int nextPoint;
    Transform player;
    bool isShot = true;

    void Start()
    {
        player = PlayerController3d.player.transform;
        shotPoint = transform.Find("shot point");
        rb = GetComponent<Rigidbody>();
        nextPoint = Random.Range(0, points.Length);
        iTween.LookTo(gameObject, iTween.Hash("looktarget", points[nextPoint].position, "axis", "y"));
        //iTween.LookTo(gameObject, player.position, 1);
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, player.position) < 15)
        {
            iTween.LookUpdate(gameObject, iTween.Hash("looktarget", player.position, "axis", "y"));
            if(isShot)
            {
                isShot = false;
                StartCoroutine(Shot());
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, points[nextPoint].position) < 2)
            {
                nextPoint = Random.Range(0, points.Length);
            }
            else
            {
                iTween.LookUpdate(gameObject, iTween.Hash("looktarget", points[nextPoint].position, "axis", "y"));
            }
        }

        rb.velocity = transform.TransformDirection(0, rb.velocity.y, speed);
    }

    IEnumerator Shot()
    {
        yield return new WaitForSeconds(0.5f);
        Rigidbody temp = Instantiate(bullet, shotPoint.position, Quaternion.identity);
        temp.AddForce(shotPoint.TransformDirection(0, 0, 1000));
        yield return new WaitForSeconds(2);
        isShot = true;
    }

    public void Damage(int damage)
    {
        hp -= damage;
        if (hp <= 0)
            Destroy(gameObject);
    }

}
