﻿// Допоміжний скріпт, знаходиться на кожному об'єкті в пулі 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObj : MonoBehaviour
{
    public int idPool;

    public void DestroyObj()
    {
        PoolSys.sys.ReturnInPool(idPool, this);
    }
}
