﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavSysNPC : MonoBehaviour
{
    public static NavSysNPC navSys;
    public NavNPC[] npcs;

    void Start()
    {
        navSys = this;   
    }

    public void NPCsos(Vector3 pos)
    {
        for(int i=0; i<npcs.Length; i++)
        {
            if(Vector3.Distance(pos, npcs[i].transform.position)<15)
            {
                npcs[i].HelpNPC(pos);
            }
        }
    }

}
