﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavNPC : MonoBehaviour
{
    public Transform[] area;
    NavMeshAgent agent;
    bool isAtack = false;
    Transform personTarget;
    Vector3 pointPatrul;
    Material mat;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        mat = GetComponent<MeshRenderer>().material;
    }

    void Start()
    {
        NextPointPatrul();
    }

    void Update()
    {
        if(!isAtack)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 10))
            {
                if(hit.collider.tag=="Player")
                {
                    personTarget = hit.transform;
                    isAtack = true;
                    NavSysNPC.navSys.NPCsos(personTarget.position);
                    mat.color = Color.red;
                }
            }
        }
        else
        {
            if(Vector3.Distance(transform.position, personTarget.position)<15)
            {
                agent.SetDestination(personTarget.position);
            }
            else
            {
                isAtack = false;
                mat.color = Color.yellow;
                agent.SetDestination(pointPatrul);
            }
        }

    }

    public void HelpNPC(Vector3 pos)
    {
        print("help");
        agent.SetDestination(pos);
    }

    void NextPointPatrul()
    {
        pointPatrul = new Vector3(Random.Range(area[0].position.x, area[1].position.x),
                Random.Range(area[0].position.y, area[1].position.y),
                Random.Range(area[0].position.z, area[1].position.z));
        agent.SetDestination(pointPatrul);
        StartCoroutine(TimerStop());
    }

    IEnumerator TimerStop()
    {
        yield return new WaitForSeconds(1);

        while (true)
        {
            yield return null;
            if (agent.velocity.magnitude < 0.2)
            {
                NextPointPatrul();
                break;
            }
        }

    }
}


