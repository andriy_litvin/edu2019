﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavPerson : MonoBehaviour
{
    public Transform sensor;
    Animator anim;
    NavMeshAgent agent;

    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();        
    }

    void Update()
    {
        anim.SetFloat("speed", agent.velocity.magnitude);

        anim.SetBool("isJamp", !Physics.Raycast(sensor.position, Vector3.down, 0.7f));

        /*if(Mathf.Abs(agent.velocity.y)>1)
        {
            anim.SetBool("isJamp", true);
        }
        else
        {
            anim.SetBool("isJamp", false);
        }*/

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                agent.isStopped = false;
                agent.SetDestination(hit.point);
                StartCoroutine(TimerStop());
            }
        }


    }

    IEnumerator TimerStop()
    {
        yield return new WaitForSeconds(1);

        while(true)
        {
            yield return null;
            if (agent.velocity.magnitude < 0.2)
            {
                agent.isStopped = true;
                break;
            }
        }

    }
}
